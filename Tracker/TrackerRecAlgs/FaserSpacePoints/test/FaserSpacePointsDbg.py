#!/usr/bin/env python
import sys
from AthenaCommon.Logging import log
from AthenaCommon.Constants import DEBUG
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from FaserSpacePoints.FaserSpacePointsConfig import FaserSpacePointsCfg

log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

ConfigFlags.Input.Files = ['my.RDO.pool.root']
ConfigFlags.Output.ESDFileName = "spacePoints.ESD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

# Core components
acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
acc.merge(FaserSpacePointsCfg(ConfigFlags))
acc.getEventAlgo("Tracker::FaserSpacePoints").OutputLevel = DEBUG

sc = acc.run(maxEvents=1000)
sys.exit(not sc.isSuccess())
