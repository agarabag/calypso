#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.TestDefaults import defaultTestFiles
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
from TrackerSegmentFit.TrackerSegmentFitConfig import SegmentFitAlgCfg
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
from FaserActsKalmanFilter.FaserActsKalmanFilterConfig import FaserActsKalmanFilterCfg


log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure
ConfigFlags.Input.Files = ['/home/tboeckh/tmp/Faser-Physics-006470-00093.raw_middleStation.SPs']
ConfigFlags.Output.ESDFileName = "MiddleStation-KalmanFilter.ESD.pool.root"
ConfigFlags.Output.AODFileName = "MiddleStation-KalmanFilter.AOD.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-03"
ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"
ConfigFlags.Input.ProjectName = "data21"
ConfigFlags.Input.isMC = False
ConfigFlags.GeoModel.FaserVersion = "FASER-02"
ConfigFlags.Common.isOnline = False
ConfigFlags.GeoModel.Align.Dynamic = False
ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.Detector.GeometryFaserSCT = True
ConfigFlags.lock()

# Core components
acc = MainServicesCfg(ConfigFlags)
acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags, name="LevelClustering", DataObjectName="SCT_LEVELMODE_RDOs", ClusterToolTimingPattern="X1X"))
acc.merge(SegmentFitAlgCfg(ConfigFlags, name=f"LevelFit", MaxClusters=44))
# acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags))
# acc.merge(SegmentFitAlgCfg(ConfigFlags))
# acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))
acc.merge(FaserActsKalmanFilterCfg(ConfigFlags))
acc.getEventAlgo("FaserActsKalmanFilterAlg").OutputLevel = DEBUG

replicaSvc = acc.getService("DBReplicaSvc")
replicaSvc.COOLSQLiteVetoPattern = ""
replicaSvc.UseCOOLSQLite = True
replicaSvc.UseCOOLFrontier = False
replicaSvc.UseGeomSQLite = True


# logging.getLogger('forcomps').setLevel(VERBOSE)
# acc.foreach_component("*").OutputLevel = VERBOSE
# acc.foreach_component("*ClassID*").OutputLevel = INFO
# acc.getService("StoreGateSvc").Dump = True
# acc.getService("ConditionStore").Dump = True
# acc.printConfig(withDetails=True)
# ConfigFlags.dump()

# Execute and finish
sc = acc.run(maxEvents=-1)
sys.exit(not sc.isSuccess())
