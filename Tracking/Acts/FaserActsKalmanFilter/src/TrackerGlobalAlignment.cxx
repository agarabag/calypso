#include "TrackerGlobalAlignment.h"

#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointCollection.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrkPrepRawData/PrepRawData.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "TrkRIO_OnTrack/RIO_OnTrack.h"
#include "TrkSurfaces/Surface.h"
#include "Identifier/Identifier.h"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsRecMultiTrajectory.h"
#include "TrackSelection.h"
#include <algorithm>

#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/Surfaces/PlaneSurface.hpp"
#include "Acts/Surfaces/RectangleBounds.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"


#include "Acts/EventData/Measurement.hpp"
#include "Acts/Definitions/Units.hpp"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TMatrixDSym.h"
#include "TMatrixD.h"
#include <fstream>
#include <iostream>


using TrajectoriesContainer = std::vector<FaserActsRecMultiTrajectory>;

using namespace Acts::UnitLiterals;

TrackerGlobalAlignment::TrackerGlobalAlignment(
    const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator), m_thistSvc("THistSvc", name) {}


  StatusCode TrackerGlobalAlignment::initialize() {
    ATH_CHECK(m_fieldCondObjInputKey.initialize());
    ATH_CHECK(m_trackingGeometryTool.retrieve());
    ATH_CHECK(m_trackCollection.initialize());
    ATH_CHECK(m_extrapolationTool.retrieve());
    ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
    CHECK(m_thistSvc.retrieve());
    m_tree= new TTree("trackParam","tree");
    initializeTree();
    CHECK(m_thistSvc->regTree("/TrackerGlobalAlignment/trackParam",m_tree));
    m_tree->AutoSave();
    return StatusCode::SUCCESS;
  }


StatusCode TrackerGlobalAlignment::execute() {
  clearVariables();
  const EventContext& ctx = Gaudi::Hive::currentContext();
  m_numberOfEvents++;

  ATH_MSG_DEBUG("get the tracking geometry");
  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
    = m_trackingGeometryTool->trackingGeometry();

  const FaserActsGeometryContext& faserActsGeometryContext = m_trackingGeometryTool->getGeometryContext();
  auto gctx = faserActsGeometryContext.context();
  Acts::MagneticFieldContext magFieldContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext;

  //read each track and get the residual and derivation
  SG::ReadHandle<TrackCollection> trackCollection {m_trackCollection, ctx}; 
  ATH_CHECK(trackCollection.isValid());
  m_numberOfFittedTracks +=trackCollection->size();
  bool save=false;

  //loop over all the track
  ATH_MSG_DEBUG("Read all the tracks "<<m_numberOfFittedTracks);
  for (const Trk::Track* track : *trackCollection)
  {
  ATH_MSG_DEBUG("loop over the track");
    if(save)break;
    if (track == nullptr || track->measurementsOnTrack()->size() < m_minNumberMeasurements || track->fitQuality()->chiSquared() > m_maxTrackChi2 ) continue;
    save=true;
  ATH_MSG_DEBUG("found good quality track");
    m_fitParam_chi2.push_back( track->fitQuality()->chiSquared() );
    m_fitParam_ndf.push_back( track->fitQuality()->numberDoF() );
    m_fitParam_nMeasurements.push_back( track->measurementsOnTrack()->size() );
    m_fitParam_nStates.push_back( track->trackStateOnSurfaces()->size() );
    m_fitParam_nOutliers.push_back( track->outliersOnTrack()->size() );

    //    m_fitParam_nHoles.push_back(ajState.nHoles);
    const Trk::TrackParameters* params = track->trackParameters()->front();

    m_fitParam_x.push_back(params->position().x());
    m_fitParam_y.push_back(params->position().y());
    m_fitParam_z.push_back(params->position().z());
    m_fitParam_charge.push_back(params->charge());
    m_fitParam_px.push_back(params->momentum().x());
    m_fitParam_py.push_back(params->momentum().y());
    m_fitParam_pz.push_back(params->momentum().z());

    //define the vector of residuals and derivations
    std::vector<double> t_align_derivation_x_x;
    std::vector<double> t_align_derivation_x_y;
    std::vector<double> t_align_derivation_x_z;
    std::vector<double> t_align_derivation_x_rx;
    std::vector<double> t_align_derivation_x_ry;
    std::vector<double> t_align_derivation_x_rz;
    std::vector<double> t_align_derivation_x_par_x;
    std::vector<double> t_align_derivation_x_par_y;
    std::vector<double> t_align_derivation_x_par_theta;
    std::vector<double> t_align_derivation_x_par_phi;
    std::vector<double> t_align_derivation_x_par_qop;
    std::vector<double> t_align_derivation_global_y_x;
    std::vector<double> t_align_derivation_global_y_y;
    std::vector<double> t_align_derivation_global_y_z;
    std::vector<double> t_align_derivation_global_y_rx;
    std::vector<double> t_align_derivation_global_y_ry;
    std::vector<double> t_align_derivation_global_y_rz;
    std::vector<double> t_align_local_residual_x;
    std::vector<double> t_align_local_measured_x;
    std::vector<double> t_align_local_measured_xe;
    std::vector<double> t_align_local_fitted_xe;
    std::vector<double> t_align_local_fitted_x;
    std::vector<double> t_align_global_measured_x;
    std::vector<double> t_align_global_measured_y;
    std::vector<double> t_align_global_measured_z;
    std::vector<double> t_align_global_measured_ye;
    std::vector<double> t_align_global_fitted_ye;
    std::vector<double> t_align_global_fitted_x;
    std::vector<double> t_align_global_fitted_y;
    std::vector<double> t_align_id;

    //convert to ACTS parameter
    const Acts::BoundTrackParameters acts_params = ATLASTrackParameterToActs(params);

    int nholes=0;
    //loop over all the measurements and get the residual
    for (auto tsos : *(track->trackStateOnSurfaces())) {
      if(tsos->type(Trk::TrackStateOnSurface::Hole)) {
	++nholes;
	continue;
      }
      const Trk::TrackParameters* tsos_params = tsos->trackParameters();
      const Acts::BoundTrackParameters parameter = ATLASTrackParameterToActs(tsos_params);
      //only use the measurement
      if(tsos_params!=nullptr&&tsos->type(Trk::TrackStateOnSurface::Measurement)){
	const Tracker::FaserSCT_ClusterOnTrack* cluster = dynamic_cast<const Tracker::FaserSCT_ClusterOnTrack*>(tsos->measurementOnTrack());
	if (cluster != nullptr) {
	  ATH_MSG_DEBUG("Track Parameter at the surface "<<tsos_params->parameters());
//	  std::cout<<"like Track Parameter at the surface "<<tsos_params->parameters()<<std::endl;;
	  Identifier id = cluster->identify();
	  int istation = m_idHelper->station(id);
	  int ilayer = m_idHelper->layer(id);
	  const auto iModuleEta = m_idHelper->eta_module(id);
	  const auto iModulePhi = m_idHelper->phi_module(id);
	  int iModule = iModulePhi;
	  if (iModuleEta < 0) iModule +=4;
	  int stereoid = m_idHelper->side(id);
	  auto trans2 = cluster->detectorElement()->surface().transform();
	  auto trans1 = trans2;
	  if(stereoid==1){
	    trans1=cluster->detectorElement()->otherSide()->surface().transform();
	    t_align_id.push_back(istation*1000+ilayer*100+iModule*10+1);
	  }
	  else
	    t_align_id.push_back(istation*1000+ilayer*100+iModule*10);

	  Amg::Vector3D global_fit(tsos_params->position().x(), tsos_params->position().y() ,tsos_params->position().z());
	  auto local_fit=trans2.inverse()*global_fit;
	  auto local_fit1=cluster->detectorElement()->surface().globalToLocal(global_fit);
	  auto local_fit2=tsos->surface().globalToLocal(global_fit);
	  //fill the positions
	  t_align_local_residual_x.push_back(cluster->localParameters()[Trk::loc1] - local_fit.x());
	  t_align_local_measured_x.push_back(cluster->localParameters()[Trk::loc1]);
	  t_align_local_measured_xe.push_back(sqrt(cluster->localCovariance()(0,0)));
//	  t_align_local_fitted_xe.push_back(tsos_params->covariance()[Trk::loc1,Trk::loc1]);
	  t_align_local_fitted_x.push_back(local_fit.x());
	  t_align_global_measured_x.push_back(cluster->globalPosition().x());
	  t_align_global_measured_y.push_back(cluster->globalPosition().y());
	  t_align_global_measured_z.push_back(cluster->globalPosition().z());
//	  t_align_global_measured_ye.push_back(sqrt(cluster->localCovariance()(0,0)));
	  t_align_global_fitted_x.push_back(tsos_params->position().x());
	  t_align_global_fitted_y.push_back(tsos_params->position().y());
	  //get derivations
	  Amg::Vector2D local_mea=cluster->localParameters() ; 
	  //derivations on track parameter
	  t_align_derivation_x_par_x.push_back(getDerivationOnParam(ctx, acts_params,  0,  trans2));
	  t_align_derivation_x_par_y.push_back(getDerivationOnParam(ctx, acts_params,  1,  trans2));
	  t_align_derivation_x_par_theta.push_back(getDerivationOnParam(ctx, acts_params,  2,  trans2));
	  t_align_derivation_x_par_phi.push_back(getDerivationOnParam(ctx, acts_params,  3,  trans2));
	  t_align_derivation_x_par_qop.push_back(getDerivationOnParam(ctx, acts_params,  4,  trans2));
	  //derivations on the alignment parameter
	  t_align_derivation_x_x.push_back(getDerivation(ctx, parameter, trans1,local_mea, 0, stereoid, trans2, 0));
	  t_align_derivation_x_y.push_back(getDerivation(ctx, parameter, trans1,local_mea, 1, stereoid, trans2, 0));
	  t_align_derivation_x_z.push_back(getDerivation(ctx, parameter, trans1,local_mea, 2, stereoid, trans2, 0));
	  t_align_derivation_x_rx.push_back(getDerivation(ctx, parameter, trans1,local_mea, 3, stereoid, trans2, 0));
	  t_align_derivation_x_ry.push_back(getDerivation(ctx, parameter, trans1,local_mea, 4, stereoid, trans2, 0));
	  t_align_derivation_x_rz.push_back(getDerivation(ctx, parameter, trans1,local_mea, 5, stereoid, trans2, 0));
	  t_align_derivation_global_y_x.push_back(getDerivation(ctx, parameter, trans1,local_mea, 0, stereoid, trans2, 1));
	  t_align_derivation_global_y_y.push_back(getDerivation(ctx, parameter, trans1,local_mea, 1, stereoid, trans2, 1));
	  t_align_derivation_global_y_z.push_back(getDerivation(ctx, parameter, trans1,local_mea, 2, stereoid, trans2, 1));
	  t_align_derivation_global_y_rx.push_back(getDerivation(ctx, parameter, trans1,local_mea, 3, stereoid, trans2, 1));
	  t_align_derivation_global_y_ry.push_back(getDerivation(ctx, parameter, trans1,local_mea, 4, stereoid, trans2, 1));
	  t_align_derivation_global_y_rz.push_back(getDerivation(ctx, parameter, trans1,local_mea, 5, stereoid, trans2, 1));
	  ATH_MSG_VERBOSE("local derivation "<<m_align_derivation_x_x.back()<<" "<<m_align_derivation_x_y.back()<<" "<<m_align_derivation_x_z.back()<<" "<<m_align_derivation_x_rx.back()<<" "<<m_align_derivation_x_ry.back()<<" "<<m_align_derivation_x_rz.back());
	  ATH_MSG_VERBOSE("global derivation "<<m_align_derivation_global_y_x.back()<<" "<<m_align_derivation_global_y_y.back()<<" "<<m_align_derivation_global_y_z.back()<<""<<m_align_derivation_global_y_rx.back()<<" "<<m_align_derivation_global_y_ry.back()<<" "<<m_align_derivation_global_y_rz.back());
	}
      }
    }

    m_fitParam_nHoles.push_back( nholes );
    
    m_align_local_residual_x.push_back(t_align_local_residual_x);
    m_align_local_measured_x.push_back(t_align_local_measured_x);
    m_align_local_measured_xe.push_back(t_align_local_measured_xe);
    m_align_local_fitted_xe.push_back(t_align_local_fitted_xe);
    m_align_local_fitted_x.push_back(t_align_local_fitted_x);
    m_align_global_measured_x.push_back(t_align_global_measured_x);
    m_align_global_measured_y.push_back(t_align_global_measured_y);
    m_align_global_measured_z.push_back(t_align_global_measured_z);
    m_align_global_measured_ye.push_back(t_align_global_measured_ye);
    m_align_global_fitted_ye.push_back(t_align_global_fitted_ye);
    m_align_global_fitted_x.push_back(t_align_global_fitted_x);
    m_align_global_fitted_y.push_back(t_align_global_fitted_y);
    m_align_id.push_back(t_align_id);
    m_align_derivation_x_x.push_back(t_align_derivation_x_x);
    m_align_derivation_x_y.push_back(t_align_derivation_x_y);
    m_align_derivation_x_z.push_back(t_align_derivation_x_z);
    m_align_derivation_x_rx.push_back(t_align_derivation_x_rx);
    m_align_derivation_x_ry.push_back(t_align_derivation_x_ry);
    m_align_derivation_x_rz.push_back(t_align_derivation_x_rz);
    m_align_derivation_x_par_x.push_back(t_align_derivation_x_par_x);
    m_align_derivation_x_par_y.push_back(t_align_derivation_x_par_y);
    m_align_derivation_x_par_theta.push_back(t_align_derivation_x_par_theta);
    m_align_derivation_x_par_phi.push_back(t_align_derivation_x_par_phi);
    m_align_derivation_x_par_qop.push_back(t_align_derivation_x_par_qop);
    m_align_derivation_global_y_x.push_back(t_align_derivation_global_y_x);
    m_align_derivation_global_y_y.push_back(t_align_derivation_global_y_y);
    m_align_derivation_global_y_z.push_back(t_align_derivation_global_y_z);
    m_align_derivation_global_y_rx.push_back(t_align_derivation_global_y_rx);
    m_align_derivation_global_y_ry.push_back(t_align_derivation_global_y_ry);
    m_align_derivation_global_y_rz.push_back(t_align_derivation_global_y_rz);
  }

  ATH_MSG_VERBOSE("finish getting residuals");

  if(save)
    m_tree->Fill();

  return StatusCode::SUCCESS;
}


StatusCode TrackerGlobalAlignment::finalize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::finalize()");
  ATH_MSG_INFO(m_numberOfEvents << " events processed.");
  ATH_MSG_INFO(m_numberOfTrackSeeds << " seeds.");
  ATH_MSG_INFO(m_numberOfFittedTracks << " fitted tracks.");
  ATH_MSG_INFO(m_numberOfSelectedTracks << " selected and re-fitted tracks.");
  /*
  //dump the matrix to a txt file for alignment
  std::vector<double>* t_fitParam_chi2=0;
  std::vector<double>* t_fitParam_pz=0;
  std::vector<double>* t_fitParam_x=0;
  std::vector<double>* t_fitParam_y=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_residual_x_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_measured_x_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_measured_xe_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_stationId_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_centery_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_layerId_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_moduleId_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_unbiased_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivation_x_x=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivation_x_y=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivation_x_z=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivation_x_rx=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivation_x_ry=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivation_x_rz=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivation_y_x=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivation_y_y=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivation_y_z=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivation_y_rx=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivation_y_ry=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivation_y_rz=0;
  std::vector<std::vector<double>>* t_fitParam_align_stereoId=0;
  std::vector<int>* t_fitParam_nMeasurements=0;
  m_tree->SetBranchAddress("fitParam_nMeasurements", &t_fitParam_nMeasurements);
  m_tree->SetBranchAddress("fitParam_align_stereoId",&t_fitParam_align_stereoId);
  m_tree->SetBranchAddress("fitParam_align_local_derivation_x_x",&t_fitParam_align_local_derivation_x_x);
  m_tree->SetBranchAddress("fitParam_align_local_derivation_x_y",&t_fitParam_align_local_derivation_x_y);
  m_tree->SetBranchAddress("fitParam_align_local_derivation_x_z",&t_fitParam_align_local_derivation_x_z);
  m_tree->SetBranchAddress("fitParam_align_local_derivation_x_rx",&t_fitParam_align_local_derivation_x_rx);
  m_tree->SetBranchAddress("fitParam_align_local_derivation_x_ry",&t_fitParam_align_local_derivation_x_ry);
  m_tree->SetBranchAddress("fitParam_align_local_derivation_x_rz",&t_fitParam_align_local_derivation_x_rz);
  m_tree->SetBranchAddress("fitParam_align_global_derivation_y_x",&t_fitParam_align_global_derivation_y_x);
  m_tree->SetBranchAddress("fitParam_align_global_derivation_y_y",&t_fitParam_align_global_derivation_y_y);
  m_tree->SetBranchAddress("fitParam_align_global_derivation_y_z",&t_fitParam_align_global_derivation_y_z);
  m_tree->SetBranchAddress("fitParam_align_global_derivation_y_rx",&t_fitParam_align_global_derivation_y_rx);
  m_tree->SetBranchAddress("fitParam_align_global_derivation_y_ry",&t_fitParam_align_global_derivation_y_ry);
  m_tree->SetBranchAddress("fitParam_align_global_derivation_y_rz",&t_fitParam_align_global_derivation_y_rz);
  m_tree->SetBranchAddress("fitParam_align_local_residual_x_sp",&t_fitParam_align_local_residual_x_sp);
  m_tree->SetBranchAddress("fitParam_chi2",&t_fitParam_chi2);
  m_tree->SetBranchAddress("fitParam_pz",&t_fitParam_pz);
  m_tree->SetBranchAddress("fitParam_x",&t_fitParam_x);
  m_tree->SetBranchAddress("fitParam_y",&t_fitParam_y);
  m_tree->SetBranchAddress("fitParam_align_local_measured_x_sp",&t_fitParam_align_local_measured_x_sp);
  m_tree->SetBranchAddress("fitParam_align_local_measured_xe_sp",&t_fitParam_align_local_measured_xe_sp);
  m_tree->SetBranchAddress("fitParam_align_layerId_sp",&t_fitParam_align_layerId_sp);
  m_tree->SetBranchAddress("fitParam_align_moduleId_sp",&t_fitParam_align_moduleId_sp);
  m_tree->SetBranchAddress("fitParam_align_unbiased_sp",&t_fitParam_align_unbiased_sp);
  m_tree->SetBranchAddress("fitParam_align_stationId_sp",&t_fitParam_align_stationId_sp);
  m_tree->SetBranchAddress("fitParam_align_centery_sp",&t_fitParam_align_centery_sp);
  int ntrk_mod[12][8]={0};
  int ntrk_sta[4]={0};
  int ntrk_lay[12]={0};
  //define matrix
  int Ndim=6;
  std::vector<TMatrixD> denom_lay;
  std::vector<TMatrixD> num_lay;
  std::vector<std::vector<TMatrixD>> denom_mod;
  std::vector<std::vector<TMatrixD>> num_mod;
  std::vector<std::vector<TMatrixD>> denom_mod1;
  std::vector<std::vector<TMatrixD>> num_mod1;
  //initialize to 0
  auto num_matrix = [](int n) {
  TMatrixD num_t(n,1);
  for(int i=0;i<n;i++){
  num_t[i][0]=0.;
  }
  return num_t;
};
auto denom_matrix = [](int n) {
  TMatrixD denom_t(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      denom_t[i][j]=0.;
    }
  }
  return denom_t;
};
std::vector<TMatrixD> denom_sta;
std::vector<TMatrixD> num_sta;
for(int i=0;i<4;i++){
  denom_sta.push_back(denom_matrix(Ndim));
  num_sta.push_back(num_matrix(Ndim));
}
for(int i=0;i<12;i++){
  denom_lay.push_back(denom_matrix(Ndim));
  num_lay.push_back(num_matrix(Ndim));
  std::vector<TMatrixD> denom_l;
  std::vector<TMatrixD> num_l;
  std::vector<TMatrixD> denom_l1;
  std::vector<TMatrixD> num_l1;
  for(int j=0;j<8;j++){
    denom_l.push_back(denom_matrix(Ndim));
    num_l.push_back(num_matrix(Ndim));
    denom_l1.push_back(denom_matrix(Ndim));
    num_l1.push_back(num_matrix(Ndim));
  }
  denom_mod.push_back(denom_l1);
  denom_mod1.push_back(denom_l1);
  num_mod.push_back(num_l);
  num_mod1.push_back(num_l1);
}
double chi_mod_y[12][8]={0};
std::cout<<"found "<<m_tree->GetEntries()<<" events "<<std::endl;
//loop over all the entries
for(int ievt=0;ievt<m_tree->GetEntries();ievt++){
  m_tree->GetEntry(ievt);
  if(ievt%10000==0)std::cout<<"processing "<<ievt<<" event"<<std::endl;
  if(t_fitParam_chi2->size()<1)continue;
  for(int i=0;i<1;i+=1){
    if(t_fitParam_chi2->at(i)>100||t_fitParam_pz->at(i)<300||sqrt(t_fitParam_x->at(i)*t_fitParam_x->at(i)+t_fitParam_y->at(i)*t_fitParam_y->at(i))>95)continue;
    if(t_fitParam_align_local_residual_x_sp->at(i).size()<15)continue;//only     use good track
    for(size_t j=0;j<t_fitParam_align_local_residual_x_sp->at(i).size();j++){
      double resx1=999.;
      double x1e=999.;
      if(m_biased&&t_fitParam_align_stationId_sp->at(i).at(j)==0){
	if(t_fitParam_align_unbiased_sp->at(i).at(j)==1){
	  resx1=(t_fitParam_align_local_residual_x_sp->at(i).at(j));
	  x1e=sqrt(t_fitParam_align_local_measured_xe_sp->at(i).at(j));
	  if(fabs(resx1)>1.0)continue;
	}
	else continue;
      }
      else{
	resx1=t_fitParam_align_local_residual_x_sp->at(i).at(j);
	x1e=sqrt(t_fitParam_align_local_measured_xe_sp->at(i).at(j));
	if(fabs(resx1)>0.2)continue;
      }
      TMatrixD m1(Ndim,1);
      m1[0][0]=t_fitParam_align_local_derivation_x_x->at(i).at(j)/x1e;
      m1[1][0]=t_fitParam_align_local_derivation_x_y->at(i).at(j)/x1e;
      m1[2][0]=t_fitParam_align_local_derivation_x_z->at(i).at(j)/x1e;
      m1[3][0]=t_fitParam_align_local_derivation_x_rx->at(i).at(j)/x1e;
      m1[4][0]=t_fitParam_align_local_derivation_x_ry->at(i).at(j)/x1e;
      m1[5][0]=t_fitParam_align_local_derivation_x_rz->at(i).at(j)/x1e;
      TMatrixD m2(1,Ndim);
      TMatrixD m3(Ndim,Ndim);
      m2.Transpose(m1);
      m3=m1*m2;
      TMatrixD m4(Ndim,1);
      m4[0][0]=t_fitParam_align_local_derivation_x_x->at(i).at(j)/x1e*resx1/x1e;
      m4[1][0]=t_fitParam_align_local_derivation_x_y->at(i).at(j)/x1e*resx1/x1e;
      m4[2][0]=t_fitParam_align_local_derivation_x_z->at(i).at(j)/x1e*resx1/x1e;
      m4[3][0]=t_fitParam_align_local_derivation_x_rx->at(i).at(j)/x1e*resx1/x1e;
      m4[4][0]=t_fitParam_align_local_derivation_x_ry->at(i).at(j)/x1e*resx1/x1e;
      m4[5][0]=t_fitParam_align_local_derivation_x_rz->at(i).at(j)/x1e*resx1/x1e;
      TMatrixD m6(Ndim,1);
      m6=m4;
      TMatrixD mg1(Ndim,1);
      mg1[0][0]=t_fitParam_align_global_derivation_y_x->at(i).at(j)/x1e;
      mg1[1][0]=t_fitParam_align_global_derivation_y_y->at(i).at(j)/x1e;
      mg1[2][0]=t_fitParam_align_global_derivation_y_z->at(i).at(j)/x1e;
      mg1[3][0]=t_fitParam_align_global_derivation_y_rx->at(i).at(j)/x1e;
      mg1[4][0]=t_fitParam_align_global_derivation_y_ry->at(i).at(j)/x1e;
      mg1[5][0]=t_fitParam_align_global_derivation_y_rz->at(i).at(j)/x1e;
      TMatrixD mg2(1,Ndim);
      TMatrixD mg3(Ndim,Ndim);
      mg2.Transpose(mg1);
      mg3=mg1*mg2;
      TMatrixD mg4(Ndim,1);
      mg4[0][0]=t_fitParam_align_global_derivation_y_x->at(i).at(j)/x1e*resx1/x1e;
      mg4[1][0]=t_fitParam_align_global_derivation_y_y->at(i).at(j)/x1e*resx1/x1e;
      mg4[2][0]=t_fitParam_align_global_derivation_y_z->at(i).at(j)/x1e*resx1/x1e;
      mg4[3][0]=t_fitParam_align_global_derivation_y_rx->at(i).at(j)/x1e*resx1/x1e;
      mg4[4][0]=t_fitParam_align_global_derivation_y_ry->at(i).at(j)/x1e*resx1/x1e;
      mg4[5][0]=t_fitParam_align_global_derivation_y_rz->at(i).at(j)/x1e*resx1/x1e;
      TMatrixD mg6(Ndim,1);
      mg6=mg4;
      int istation=t_fitParam_align_stationId_sp->at(i).at(j);
      int ilayer=t_fitParam_align_layerId_sp->at(i).at(j);
      int imodule=t_fitParam_align_moduleId_sp->at(i).at(j);
      //station: 1,2,3
      if(istation>=0&&istation<4){
	denom_sta[istation]+=mg3;
	num_sta[istation]+=mg6;
	ntrk_sta[istation]+=1;
      }
      int ilayer_tot = ilayer + (istation )*3;
      chi_mod_y[ilayer_tot][imodule]+=0;
      if(ilayer_tot>=0&&ilayer_tot<12){
	denom_lay[ilayer_tot]+=mg3;
	num_lay[ilayer_tot]+=mg6;
	ntrk_lay[ilayer_tot]+=1;
	denom_mod[ilayer_tot][imodule]+=m3;
	num_mod[ilayer_tot][imodule]+=m6;
	ntrk_mod[ilayer_tot][imodule]+=1;
      }
    }
  }
}

std::cout<<"Get the nominal transform"<<std::endl;
//get the alignment constants
std::ofstream align_output("alignment_matrix.txt",std::ios::out);
for(int i=0;i<4;i++){
  for(int ii=0;ii<Ndim;ii++){
    align_output<<i<<" "<<ii<<" "<<denom_sta[i][ii][0]<<" "<<denom_sta[i][ii][1]<<" "<<denom_sta[i][ii][2]<<" "<<denom_sta[i][ii][3]<<" "<<denom_sta[i][ii][4]<<" "<<denom_sta[i][ii][5]<<" "<<0<<std::endl;
    if(ii==0)
      std::cout<<i<<" "<<ii<<" "<<denom_sta[i][ii][0]<<" "<<denom_sta[i][ii][1]<<" "<<denom_sta[i][ii][2]<<" "<<denom_sta[i][ii][3]<<" "<<denom_sta[i][ii][4]<<" "<<denom_sta[i][ii][5]<<" "<<0<<std::endl;
  }
  align_output<<i<<" "<<6<<" "<<num_sta[i][0][0]<<" "<<num_sta[i][1][0]<<" "<<num_sta[i][2][0]<<" "<<num_sta[i][3][0]<<" "<<num_sta[i][4][0]<<" "<<num_sta[i][5][0]<<" "<<ntrk_sta[i]<<std::endl;
}
std::cout<<"Get the deltas for stations"<<std::endl;
//layers
for(size_t i=0;i<denom_lay.size();i++){
  for(int ii=0;ii<Ndim;ii++){
    align_output<<i/3<<i%3<<" "<<ii<<" "<<denom_lay[i][ii][0]<<" "<<denom_lay[i][ii][1]<<" "<<denom_lay[i][ii][2]<<" "<<denom_lay[i][ii][3]<<" "<<denom_lay[i][ii][4]<<" "<<denom_lay[i][ii][5]<<" "<<0<<std::endl;
  }
  align_output<<i/3<<i%3<<" "<<6<<" "<<num_lay[i][0][0]<<" "<<num_lay[i][1][0]<<" "<<num_lay[i][2][0]<<" "<<num_lay[i][3][0]<<" "<<num_lay[i][4][0]<<" "<<num_lay[i][5][0]<<" "<<ntrk_lay[i]<<std::endl;
}

std::cout<<"Get the deltas for layers"<<std::endl;
for(size_t i=0;i<denom_lay.size();i++){
  for(  size_t j=0;j<num_mod[i].size();j++){
    for(int ii=0;ii<Ndim;ii++){
      align_output<<i/3<<i%3<<j<<" "<<ii<<" "<<denom_mod[i][j][ii][0]<<" "<<denom_mod[i][j][ii][1]<<" "<<denom_mod[i][j][ii][2]<<" "<<denom_mod[i][j][ii][3]<<" "<<denom_mod[i][j][ii][4]<<" "<<denom_mod[i][j][ii][5]<<" "<<chi_mod_y[i][j]<<std::endl;
    }
    align_output<<i/3<<i%3<<j<<" "<<6<<" "<<num_mod[i][j][0][0]<<" "<<num_mod[i][j][1][0]<<" "<<num_mod[i][j][2][0]<<" "<<num_mod[i][j][3][0]<<" "<<num_mod[i][j][4][0]<<" "<<num_mod[i][j][5][0]<<" "<<ntrk_mod[i][j]<<std::endl;
  }
}
std::cout<<"closing the output"<<std::endl;
align_output.close();
*/
return StatusCode::SUCCESS;
}


Acts::MagneticFieldContext TrackerGlobalAlignment::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};
  return Acts::MagneticFieldContext(fieldCondObj);
}



void TrackerGlobalAlignment::initializeTree(){
  m_tree->Branch("evtId",&m_numberOfEvents);
  m_tree->Branch("fitParam_x", &m_fitParam_x);
  m_tree->Branch("fitParam_charge", &m_fitParam_charge);
  m_tree->Branch("fitParam_y", &m_fitParam_y);
  m_tree->Branch("fitParam_z", &m_fitParam_z);
  m_tree->Branch("fitParam_px", &m_fitParam_px);
  m_tree->Branch("fitParam_py", &m_fitParam_py);
  m_tree->Branch("fitParam_pz", &m_fitParam_pz);
  m_tree->Branch("fitParam_chi2", &m_fitParam_chi2);
  m_tree->Branch("fitParam_ndf", &m_fitParam_ndf);
  m_tree->Branch("fitParam_nHoles", &m_fitParam_nHoles);
  m_tree->Branch("fitParam_nOutliers", &m_fitParam_nOutliers);
  m_tree->Branch("fitParam_nStates", &m_fitParam_nStates);
  m_tree->Branch("fitParam_nMeasurements", &m_fitParam_nMeasurements);
  m_tree->Branch("fitParam_align_id", &m_align_id);
  m_tree->Branch("fitParam_align_local_derivation_x_par_x", &m_align_derivation_x_par_x);
  m_tree->Branch("fitParam_align_local_derivation_x_par_y", &m_align_derivation_x_par_y);
  m_tree->Branch("fitParam_align_local_derivation_x_par_theta", &m_align_derivation_x_par_theta);
  m_tree->Branch("fitParam_align_local_derivation_x_par_phi", &m_align_derivation_x_par_phi);
  m_tree->Branch("fitParam_align_local_derivation_x_par_qop", &m_align_derivation_x_par_qop);
  m_tree->Branch("fitParam_align_local_derivation_x_x", &m_align_derivation_x_x);
  m_tree->Branch("fitParam_align_local_derivation_x_y", &m_align_derivation_x_y);
  m_tree->Branch("fitParam_align_local_derivation_x_z", &m_align_derivation_x_z);
  m_tree->Branch("fitParam_align_local_derivation_x_rx", &m_align_derivation_x_rx);
  m_tree->Branch("fitParam_align_local_derivation_x_ry", &m_align_derivation_x_ry);
  m_tree->Branch("fitParam_align_local_derivation_x_rz", &m_align_derivation_x_rz);
  m_tree->Branch("fitParam_align_global_derivation_y_x", &m_align_derivation_global_y_x);
  m_tree->Branch("fitParam_align_global_derivation_y_y", &m_align_derivation_global_y_y);
  m_tree->Branch("fitParam_align_global_derivation_y_z", &m_align_derivation_global_y_z);
  m_tree->Branch("fitParam_align_global_derivation_y_rx", &m_align_derivation_global_y_rx);
  m_tree->Branch("fitParam_align_global_derivation_y_ry", &m_align_derivation_global_y_ry);
  m_tree->Branch("fitParam_align_global_derivation_y_rz", &m_align_derivation_global_y_rz);
  m_tree->Branch("fitParam_align_local_residual_x", &m_align_local_residual_x);
  m_tree->Branch("fitParam_align_local_measured_x", &m_align_local_measured_x);
  m_tree->Branch("fitParam_align_local_measured_xe", &m_align_local_measured_xe);
  m_tree->Branch("fitParam_align_local_fitted_xe", &m_align_local_fitted_xe);
  m_tree->Branch("fitParam_align_local_fitted_x", &m_align_local_fitted_x);
  m_tree->Branch("fitParam_align_global_measured_x", &m_align_global_measured_x);
  m_tree->Branch("fitParam_align_global_measured_y", &m_align_global_measured_y);
  m_tree->Branch("fitParam_align_global_measured_ye", &m_align_global_measured_ye);
  m_tree->Branch("fitParam_align_global_measured_z", &m_align_global_measured_z);
  m_tree->Branch("fitParam_align_global_fitted_ye", &m_align_global_fitted_ye);
  m_tree->Branch("fitParam_align_global_fitted_x", &m_align_global_fitted_x);
  m_tree->Branch("fitParam_align_global_fitted_y", &m_align_global_fitted_y);
}

void TrackerGlobalAlignment::clearVariables(){
    m_fitParam_x.clear();
    m_fitParam_charge.clear();
    m_fitParam_y.clear();
    m_fitParam_z.clear();
    m_fitParam_px.clear();
    m_fitParam_py.clear();
    m_fitParam_pz.clear();
    m_fitParam_chi2.clear();
    m_fitParam_ndf.clear();
    m_fitParam_nHoles.clear();
    m_fitParam_nOutliers.clear();
    m_fitParam_nStates.clear();
    m_fitParam_nMeasurements.clear();
  m_align_derivation_x_x.clear();
  m_align_derivation_x_y.clear();
  m_align_derivation_x_z.clear();
  m_align_derivation_x_rx.clear();
  m_align_derivation_x_ry.clear();
  m_align_derivation_x_rz.clear();
  m_align_derivation_x_par_x.clear();
  m_align_derivation_x_par_y.clear();
  m_align_derivation_x_par_theta.clear();
  m_align_derivation_x_par_phi.clear();
  m_align_derivation_x_par_qop.clear();
  m_align_derivation_global_y_x.clear();
  m_align_derivation_global_y_y.clear();
  m_align_derivation_global_y_z.clear();
  m_align_derivation_global_y_rx.clear();
  m_align_derivation_global_y_ry.clear();
  m_align_derivation_global_y_rz.clear();
  m_align_local_residual_x.clear();
  m_align_local_measured_x.clear();
  m_align_local_measured_xe.clear();
  m_align_local_fitted_xe.clear();
  m_align_local_fitted_x.clear();
  m_align_global_measured_x.clear();
  m_align_global_measured_y.clear();
  m_align_global_measured_z.clear();
  m_align_global_measured_ye.clear();
  m_align_global_fitted_ye.clear();
  m_align_global_fitted_x.clear();
  m_align_global_fitted_y.clear();
  m_align_id.clear();
}


double TrackerGlobalAlignment::getLocalDerivation(Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2)const {
  double delta[6]={0.001,0.001,0.001,0.0001,0.0001,0.0001};
  auto translation_m = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_m= translation_m* Amg::RotationMatrix3D::Identity();
  auto translation_p = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_p= translation_p* Amg::RotationMatrix3D::Identity();
  switch (ia)
  {
    case 0:
      transform_m *= Amg::Translation3D(0-delta[ia],0,0);
      transform_p *= Amg::Translation3D(0+delta[ia],0,0);
      break;
    case 1:
      transform_m *= Amg::Translation3D(0,0-delta[ia],0);
      transform_p *= Amg::Translation3D(0,0+delta[ia],0);
      break;
    case 2:
      transform_m *= Amg::Translation3D(0,0,0-delta[ia]);
      transform_p *= Amg::Translation3D(0,0,0+delta[ia]);
      break;
    case 3:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(1,0,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(1,0,0));
      break;
    case 4:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,1,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,1,0));
      break;
    case 5:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,0,1));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,0,1));
      break;
    default:
      ATH_MSG_FATAL("Unknown alignment parameter" );
      break;
  }

  auto local3 = Amg::Vector3D(loc_pos.x(),loc_pos.y(),0.);
  Amg::Vector3D glo_pos = trans1 * local3;
  Amg::Vector3D local_m = (trans1 * transform_m).inverse()*glo_pos;
  Amg::Vector3D local_p = (trans1 * transform_p).inverse()*glo_pos;
  if(side!=1)
    return (local_p.x()-local_m.x())/2./delta[ia];
  else{
    Amg::Vector3D local_m2=trans2.inverse()*trans1*local_m;
    Amg::Vector3D local_p2=trans2.inverse()*trans1*local_p;
    return (local_p2.x()-local_m2.x())/2./delta[ia];
  }
}


double TrackerGlobalAlignment::getDerivation(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2, int global)const {
  double delta[6]={0.001,0.001,0.001,0.0001,0.0001,0.0001};
  auto translation_m = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_m= translation_m* Amg::RotationMatrix3D::Identity();
  auto translation_p = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_p= translation_p* Amg::RotationMatrix3D::Identity();
  switch (ia)
  {
    case 0:
      transform_m *= Amg::Translation3D(0-delta[ia],0,0);
      transform_p *= Amg::Translation3D(0+delta[ia],0,0);
      break;
    case 1:
      transform_m *= Amg::Translation3D(0,0-delta[ia],0);
      transform_p *= Amg::Translation3D(0,0+delta[ia],0);
      break;
    case 2:
      transform_m *= Amg::Translation3D(0,0,0-delta[ia]);
      transform_p *= Amg::Translation3D(0,0,0+delta[ia]);
      break;
    case 3:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(1,0,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(1,0,0));
      break;
    case 4:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,1,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,1,0));
      break;
    case 5:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,0,1));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,0,1));
      break;
    default:
      ATH_MSG_FATAL("Unknown alignment parameter" );
      break;
  }

  auto local3 = Amg::Vector3D(loc_pos.x(),loc_pos.y(),0.);
  Amg::Vector3D glo_pos = trans2 * local3;
  auto trans_m = trans1 * transform_m;
  auto trans_p = trans1 * transform_p;
  if(side==1 && global!=1){
    trans_m = trans1 * transform_m * trans1.inverse() * trans2;
    trans_p = trans1 * transform_p * trans1.inverse() * trans2;
  }
  if (global==1){
    Amg::Transform3D trans_g = Amg::Translation3D(0,0, glo_pos.z()) * Amg::RotationMatrix3D::Identity();
    auto trans_l2g = trans_g.inverse()*trans1;
    trans_m = trans1 * trans_l2g.inverse() * transform_m * trans_l2g;
    trans_p = trans1 * trans_l2g.inverse() * transform_p * trans_l2g;
    if(side==1){
      auto tmpm = trans_m;
      auto tmpp = trans_p;
      trans_m = tmpm * trans1.inverse() * trans2;
      trans_p = tmpp * trans1.inverse() * trans2;
    }
  }

  Amg::Transform3D ini_trans = Amg::Translation3D(0,0, glo_pos.z()-10) * Amg::RotationMatrix3D::Identity();
  auto ini_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(ini_trans, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  std::unique_ptr<const Acts::BoundTrackParameters> ini_Param = m_extrapolationTool->propagate( ctx, fitParameters, *ini_surface, Acts::backward);
  if(ini_Param==nullptr)return -9999.;


  auto shift_m_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans_m, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  auto shift_p_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans_p, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  auto shift_m_Param = m_extrapolationTool->propagate( ctx, *ini_Param, *shift_m_surface, Acts::forward);
  auto shift_p_Param = m_extrapolationTool->propagate( ctx, *ini_Param, *shift_p_surface, Acts::forward);
  if(shift_m_Param!=nullptr&&shift_p_Param!=nullptr){
    auto shift_m_parameter = shift_m_Param->parameters();
    auto shift_p_parameter = shift_p_Param->parameters();
    Amg::Vector2D local_m(shift_m_parameter[Acts::eBoundLoc0],shift_m_parameter[Acts::eBoundLoc1]);
    Amg::Vector2D local_p(shift_p_parameter[Acts::eBoundLoc0],shift_p_parameter[Acts::eBoundLoc1]);
    return (local_p.x()-local_m.x())/2./delta[ia];
  }
  return -99999.;
}

double TrackerGlobalAlignment::getDerivationOnParam(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, int ia, Amg::Transform3D trans2)const {

  double delta[5]={0.005,0.005,0.00001,0.00001,1.};
  auto target_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans2, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  Acts::BoundTrackParameters::ParametersVector param_m = fitParameters.parameters();
  Acts::BoundTrackParameters::ParametersVector param_p = fitParameters.parameters();
  double tmp_momentum = 1./param_m[Acts::eBoundQOverP];
  switch (ia)
  {
    case 0:
      param_m[Acts::eBoundLoc0] -= delta[ia];
      param_p[Acts::eBoundLoc0] += delta[ia];
      break;
    case 1:
      param_m[Acts::eBoundLoc1] -= delta[ia];
      param_p[Acts::eBoundLoc1] += delta[ia];
      break;
    case 2:
      param_m[Acts::eBoundTheta] -= delta[ia];
      param_p[Acts::eBoundTheta] += delta[ia];
      break;
    case 3:
      param_m[Acts::eBoundPhi] -= delta[ia];
      param_p[Acts::eBoundPhi] += delta[ia];
      break;
    case 4:
      //if the qop is almost 0 then use a smaller delta
      //      if(fabs(param_m[Acts::eBoundQOverP])<2*delta[ia]){
      //	delta[ia]= 0.01*fabs(param_m[Acts::eBoundQOverP]);
      //	param_m[Acts::eBoundQOverP] -= delta[ia];
      //	param_p[Acts::eBoundQOverP] += delta[ia];
      //      }
      //      else{
      //	param_m[Acts::eBoundQOverP] -= delta[ia];
      //	param_p[Acts::eBoundQOverP] += delta[ia];
      //      }
      param_m[Acts::eBoundQOverP] = 1./(tmp_momentum-delta[ia]);
      param_p[Acts::eBoundQOverP] = 1./(tmp_momentum+delta[ia]);
      break;
    default:
      ATH_MSG_FATAL("Unknown track parameter" );
      break;
  }
  //  std::cout<<"ini params:   " << fitParameters.parameters().transpose()<<std::endl;
  //  std::cout<<"m params:   " << param_m.transpose()<<std::endl;
  //  std::cout<<"p params:   " << param_p.transpose()<<std::endl;

  Acts::BoundTrackParameters shift_m_ini_parameter( fitParameters.referenceSurface().getSharedPtr(), param_m, fitParameters.covariance());
  Acts::BoundTrackParameters shift_p_ini_parameter( fitParameters.referenceSurface().getSharedPtr(), param_p, fitParameters.covariance());


  auto shift_m_Param = m_extrapolationTool->propagate( ctx, shift_m_ini_parameter, *target_surface, Acts::forward);
  auto shift_p_Param = m_extrapolationTool->propagate( ctx, shift_p_ini_parameter, *target_surface, Acts::forward);
  if(shift_m_Param!=nullptr&&shift_p_Param!=nullptr){
    auto shift_m_parameter = shift_m_Param->parameters();
    auto shift_p_parameter = shift_p_Param->parameters();
    Amg::Vector2D local_m(shift_m_parameter[Acts::eBoundLoc0],shift_m_parameter[Acts::eBoundLoc1]);
    Amg::Vector2D local_p(shift_p_parameter[Acts::eBoundLoc0],shift_p_parameter[Acts::eBoundLoc1]);
    return (local_p.x()-local_m.x())/2./delta[ia];
  }
  return -99999.;
}

const Acts::BoundTrackParameters
TrackerGlobalAlignment::ATLASTrackParameterToActs(const Trk::TrackParameters *atlasParameter) const {

  using namespace Acts::UnitLiterals;
  std::shared_ptr<const Acts::Surface> actsSurface;
  Acts::BoundVector params;

  // get the associated surface
  if (atlasParameter->hasSurface())
  {
    const Trk::Surface& atlassurf = atlasParameter->associatedSurface();
  actsSurface = Acts::Surface::makeShared<Acts::PlaneSurface>(atlassurf.transform(), std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  }

  // Construct track parameters
  auto atlasParam = atlasParameter->parameters();
  params << atlasParam[Trk::locX], atlasParam[Trk::locY], atlasParam[Trk::phi0], atlasParam[Trk::theta], atlasParameter->charge() / (atlasParameter->momentum().mag() * 1_MeV), 0.;

  Acts::BoundSymMatrix cov = Acts::BoundSymMatrix::Identity();
  cov.topLeftCorner(5, 5) = *atlasParameter->covariance();

  // Convert the covariance matrix to MeV
  for(int i=0; i < cov.rows(); i++){
    cov(i, 4) = cov(i, 4)/1_MeV;
  }
  for(int i=0; i < cov.cols(); i++){
    cov(4, i) = cov(4, i)/1_MeV;
  }

  return Acts::BoundTrackParameters(actsSurface, params, atlasParameter->charge(), cov);
}
