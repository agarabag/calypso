#ifndef FASERACTSKALMANFILTER_CKF2GLOBALALIGNMENT_H
#define FASERACTSKALMANFILTER_CKF2GLOBALALIGNMENT_H


#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointContainer.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsGeometryInterfaces/IFaserActsExtrapolationTool.h"
#include "Acts/TrackFitting/KalmanFitter.hpp"
#include "Acts/TrackFinding/CombinatorialKalmanFilter.hpp"
#include "Acts/TrackFinding/MeasurementSelector.hpp"
#include "FaserActsKalmanFilter/Measurement.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "TrkTrack/TrackCollection.h"
#include "FaserActsKalmanFilter/ITrackSeedTool.h"
#include "KalmanFitterTool.h"
#include <boost/dynamic_bitset.hpp>
#include "GaudiKernel/ITHistSvc.h"
#include "CreateTrkTrackTool.h"

using ConstTrackStateProxy = Acts::detail_lt::TrackStateProxy<IndexSourceLink, 6, true>;
using ClusterSet = boost::dynamic_bitset<>;

class TTree;
class FaserSCT_ID;


namespace TrackerDD {
  class SCT_DetectorManager;
}

class CKF2GlobalAlignment : public AthAlgorithm {
  public:
    CKF2GlobalAlignment(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~CKF2GlobalAlignment() = default;

    StatusCode initialize() override;
    StatusCode execute() override;
    StatusCode finalize() override;
    void initializeTree();
    void clearVariables();

    using TrackFinderOptions =
      Acts::CombinatorialKalmanFilterOptions<IndexSourceLinkAccessor,
      MeasurementCalibrator,
      Acts::MeasurementSelector>;
    using CKFResult = Acts::CombinatorialKalmanFilterResult<IndexSourceLink>;
    using TrackFitterResult = Acts::Result<CKFResult>;
    using TrackFinderResult = std::vector<TrackFitterResult>;

    using KFResult =
      Acts::Result<Acts::KalmanFitterResult<IndexSourceLink>>;

    using TrackFitterOptions =
      Acts::KalmanFitterOptions<MeasurementCalibrator, Acts::VoidOutlierFinder,
      Acts::VoidReverseFilteringLogic>;

    using TrackParameters = Acts::CurvilinearTrackParameters;
    using TrackParametersContainer = std::vector<TrackParameters>;

    // Track Finding
    class TrackFinderFunction {
      public:
	virtual ~TrackFinderFunction() = default;
	virtual TrackFinderResult operator()(const IndexSourceLinkContainer&,
	    const TrackParametersContainer&,
	    const TrackFinderOptions&) const = 0;
    };

    static std::shared_ptr<TrackFinderFunction> makeTrackFinderFunction(
	std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry,
	bool resolvePassive, bool resolveMaterial, bool resolveSensitive);

    // Track Fitting
    class TrackFitterFunction {
      public:
	virtual ~TrackFitterFunction() = default;
	virtual KFResult operator()(const std::vector<IndexSourceLink>&,
	    const Acts::BoundTrackParameters&,
	    const TrackFitterOptions&) const = 0;
    };

    struct TrajectoryInfo {
      TrajectoryInfo(const FaserActsRecMultiTrajectory &traj) :
	trajectory{traj}, clusterSet{nClusters} {
	  auto state = Acts::MultiTrajectoryHelpers::trajectoryState(traj.multiTrajectory(), traj.tips().front());
	  traj.multiTrajectory().visitBackwards(traj.tips().front(), [&](const ConstTrackStateProxy& state) {
	      auto typeFlags = state.typeFlags();
	      if (not typeFlags.test(Acts::TrackStateFlag::MeasurementFlag)) {
	      return true;
	      }
	      clusterSet.set(state.uncalibrated().index());
	      return true;
	      });
	  nMeasurements = state.nMeasurements;
	  chi2 = state.chi2Sum;
	}

      static size_t nClusters;
      FaserActsRecMultiTrajectory trajectory;
      ClusterSet clusterSet;
      size_t nMeasurements;
      double chi2;
    };

    static std::shared_ptr<TrackFitterFunction> makeTrackFitterFunction(
	std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry);

    virtual Acts::MagneticFieldContext getMagneticFieldContext(const EventContext& ctx) const;

  private:
    int  m_numberOfEvents {0};
    int  m_numberOfTrackSeeds {0};
    int  m_numberOfFittedTracks {0};
    int  m_numberOfSelectedTracks {0};

    void computeSharedHits(std::vector<IndexSourceLink>* sourceLinks, TrackFinderResult& results) const;
    std::shared_ptr<TrackFinderFunction> m_fit;
    std::shared_ptr<TrackFitterFunction> m_kf;
    std::unique_ptr<const Acts::Logger> m_logger;
    const FaserSCT_ID* m_idHelper {nullptr};
    const TrackerDD::SCT_DetectorManager* m_detManager {nullptr};

    Gaudi::Property<bool> m_biased{this, "BiasedResidual", true};
    Gaudi::Property<std::string> m_actsLogging {this, "ActsLogging", "VERBOSE"};
    Gaudi::Property<int> m_minNumberMeasurements {this, "MinNumberMeasurements", 12};
    Gaudi::Property<bool> m_resolvePassive {this, "resolvePassive", false};
    Gaudi::Property<bool> m_resolveMaterial {this, "resolveMaterial", true};
    Gaudi::Property<bool> m_resolveSensitive {this, "resolveSensitive", true};
    Gaudi::Property<double> m_maxSteps {this, "maxSteps", 10000};
    Gaudi::Property<double> m_chi2Max {this, "chi2Max", 15};
    Gaudi::Property<unsigned long> m_nMax {this, "nMax", 10};
    SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};
    ToolHandle<ITrackSeedTool> m_trackSeedTool {this, "TrackSeed", "ClusterTrackSeedTool"};
    ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool {this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
    ToolHandle<KalmanFitterTool> m_kalmanFitterTool1 {this, "KalmanFitterTool1", "KalmanFitterTool"};
    ToolHandle<CreateTrkTrackTool> m_createTrkTrackTool {this, "CreateTrkTrackTool", "CreateTrkTrackTool"};
    Gaudi::Property<bool> m_isMC {this, "isMC", false};
    ToolHandle<IFaserActsExtrapolationTool> m_extrapolationTool{this, "ExtrapolationTool", "FaserActsExtrapolationTool"};
    SG::ReadHandleKey<Tracker::FaserSCT_ClusterContainer> m_clusterContainerKey { this, "ClusterContainer", "SCT_ClusterContainer"};
    SG::ReadHandleKey<FaserSCT_SpacePointContainer> m_spacePointContainerKey {this, "SpacePoints", "SCT_SpacePointContainer"};

    double getDerivative(const EventContext& ctx, Acts::BoundTrackParameters& fitParameters, Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2, int global)const ;
    double getDerivativeOnParam(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, int ia, Amg::Transform3D trans2)const ;
    double getLocalDerivative(Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2)const;
     
    std::pair<double, double> IFTgetDerivativeOnParam(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, int ia, Amg::Transform3D trans2)const;
    std::pair<double, double> IFTgetDerivative(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int global)const;

    //output ntuple
    ServiceHandle<ITHistSvc>  m_thistSvc;
    TTree* m_tree;
    mutable int m_eventId=0;
    mutable int m_trackId=0;
    mutable int m_runId=0;
    mutable double m_eventtime=0;
    mutable int m_bcid=0;
    mutable double m_fitParam_x;
    mutable double m_fitParam_charge;
    mutable double m_fitParam_y;
    mutable double m_fitParam_z;
    mutable double m_fitParam_px;
    mutable double m_fitParam_py;
    mutable double m_fitParam_pz;
    mutable double m_fitParam_chi2;
    mutable int m_fitParam_ndf;
    mutable int m_fitParam_nHoles;
    mutable int m_fitParam_nOutliers;
    mutable int m_fitParam_nStates;
    mutable int m_fitParam_nMeasurements;
    mutable std::vector<double> m_ift_id;
    mutable std::vector<double> m_ift_s0_strip;
    mutable std::vector<double> m_ift_s1_strip;
    mutable std::vector<double> m_ift_local_x;
    mutable std::vector<double> m_ift_local_y;
    mutable std::vector<double> m_ift_local_xe;
    mutable std::vector<double> m_ift_local_ye;
    mutable std::vector<double> m_ift_local_residual_x;
    mutable std::vector<double> m_ift_local_residual_y;
    mutable std::vector<double> m_ift_global_x;
    mutable std::vector<double> m_ift_global_y;
    mutable std::vector<double> m_ift_global_z;
    mutable std::vector<double> m_ift_fit_local_x;
    mutable std::vector<double> m_ift_fit_local_y;
    mutable std::vector<double> m_ift_fit_global_x;
    mutable std::vector<double> m_ift_fit_global_y;
    mutable std::vector<double> m_ift_fit_global_z;

    mutable std::vector<double> m_ift_clus_id;
    mutable std::vector<double> m_ift_clus_strip;
    mutable std::vector<double> m_ift_clus_local_x;
    mutable std::vector<double> m_ift_clus_local_y;
    mutable std::vector<double> m_ift_clus_global_x;
    mutable std::vector<double> m_ift_clus_global_y;
    mutable std::vector<double> m_ift_clus_global_z;
    mutable std::vector<double> m_ift_clus_fit_local_x;
    mutable std::vector<double> m_ift_clus_fit_local_y;
    mutable std::vector<double> m_ift_clus_fit_global_x;
    mutable std::vector<double> m_ift_clus_fit_global_y;
    mutable std::vector<double> m_ift_clus_fit_global_z;

    mutable std::vector<double> m_align_id;
    mutable std::vector<double> m_align_global_measured_x;
    mutable std::vector<double> m_align_global_measured_y;
    mutable std::vector<double> m_align_global_measured_z;
    mutable std::vector<double> m_align_global_measured_ye;
    mutable std::vector<double> m_align_global_fitted_ye;
    mutable std::vector<double> m_align_global_fitted_y;
    mutable std::vector<double> m_align_global_fitted_x;
    mutable std::vector<double> m_align_local_residual_x;
    mutable std::vector<double> m_align_local_measured_x;
    mutable std::vector<double> m_align_local_measured_xe;
    mutable std::vector<double> m_align_local_fitted_xe;
    mutable std::vector<double> m_align_local_fitted_x;
    mutable std::vector<double> m_align_derivative_x_x;
    mutable std::vector<double> m_align_derivative_x_y;
    mutable std::vector<double> m_align_derivative_x_z;
    mutable std::vector<double> m_align_derivative_x_rx;
    mutable std::vector<double> m_align_derivative_x_ry;
    mutable std::vector<double> m_align_derivative_x_rz;
    mutable std::vector<double> m_align_derivative_x_par_x;
    mutable std::vector<double> m_align_derivative_x_par_y;
    mutable std::vector<double> m_align_derivative_x_par_theta;
    mutable std::vector<double> m_align_derivative_x_par_phi;
    mutable std::vector<double> m_align_derivative_x_par_qop;
    mutable std::vector<double> m_align_derivative_global_y_x;
    mutable std::vector<double> m_align_derivative_global_y_y;
    mutable std::vector<double> m_align_derivative_global_y_z;
    mutable std::vector<double> m_align_derivative_global_y_rx;
    mutable std::vector<double> m_align_derivative_global_y_ry;
    mutable std::vector<double> m_align_derivative_global_y_rz;
    // for IFT:
    mutable std::vector<double> m_ift_align_derivative_x_x;
    mutable std::vector<double> m_ift_align_derivative_x_y;
    mutable std::vector<double> m_ift_align_derivative_x_z;
    mutable std::vector<double> m_ift_align_derivative_x_rx;
    mutable std::vector<double> m_ift_align_derivative_x_ry;
    mutable std::vector<double> m_ift_align_derivative_x_rz;
    mutable std::vector<double> m_ift_align_derivative_x_par_x;
    mutable std::vector<double> m_ift_align_derivative_x_par_y;
    mutable std::vector<double> m_ift_align_derivative_x_par_theta;
    mutable std::vector<double> m_ift_align_derivative_x_par_phi;
    mutable std::vector<double> m_ift_align_derivative_x_par_qop;
    mutable std::vector<double> m_ift_align_derivative_global_y_x;
    mutable std::vector<double> m_ift_align_derivative_global_y_y;
    mutable std::vector<double> m_ift_align_derivative_global_y_z;
    mutable std::vector<double> m_ift_align_derivative_global_y_rx;
    mutable std::vector<double> m_ift_align_derivative_global_y_ry;
    mutable std::vector<double> m_ift_align_derivative_global_y_rz;

    mutable std::vector<double> m_ift_align_derivative_y_x;
    mutable std::vector<double> m_ift_align_derivative_y_y;
    mutable std::vector<double> m_ift_align_derivative_y_z;
    mutable std::vector<double> m_ift_align_derivative_y_rx;
    mutable std::vector<double> m_ift_align_derivative_y_ry;
    mutable std::vector<double> m_ift_align_derivative_y_rz;
    mutable std::vector<double> m_ift_align_derivative_y_par_x;
    mutable std::vector<double> m_ift_align_derivative_y_par_y;
    mutable std::vector<double> m_ift_align_derivative_y_par_theta;
    mutable std::vector<double> m_ift_align_derivative_y_par_phi;
    mutable std::vector<double> m_ift_align_derivative_y_par_qop;
    mutable std::vector<double> m_ift_align_derivative_global_x_x;
    mutable std::vector<double> m_ift_align_derivative_global_x_y;
    mutable std::vector<double> m_ift_align_derivative_global_x_z;
    mutable std::vector<double> m_ift_align_derivative_global_x_rx;
    mutable std::vector<double> m_ift_align_derivative_global_x_ry;
    mutable std::vector<double> m_ift_align_derivative_global_x_rz;
};

#endif // FASERACTSKALMANFILTER_CKF2GlobalAlignment_H

