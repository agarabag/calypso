#ifndef FASERACTSKALMANFILTER_TRACKERGLOBALALIGNMENT_H
#define FASERACTSKALMANFILTER_TRACKERGLOBALALIGNMENT_H


#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsGeometryInterfaces/IFaserActsExtrapolationTool.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "TrkTrack/TrackCollection.h"
#include "KalmanFitterTool.h"
#include <boost/dynamic_bitset.hpp>
#include "GaudiKernel/ITHistSvc.h"


class TTree;
class FaserSCT_ID;


namespace TrackerDD {
  class SCT_DetectorManager;
}

class TrackerGlobalAlignment : public AthAlgorithm {
  public:
    TrackerGlobalAlignment(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~TrackerGlobalAlignment() = default;

    StatusCode initialize() override;
    StatusCode execute() override;
    StatusCode finalize() override;
    void initializeTree();
    void clearVariables();

    using TrackParameters = Acts::CurvilinearTrackParameters;
    using TrackParametersContainer = std::vector<TrackParameters>;

    virtual Acts::MagneticFieldContext getMagneticFieldContext(const EventContext& ctx) const;

  private:
    int  m_numberOfEvents {0};
    int  m_numberOfTrackSeeds {0};
    int  m_numberOfFittedTracks {0};
    int  m_numberOfSelectedTracks {0};

    const FaserSCT_ID* m_idHelper {nullptr};
    const TrackerDD::SCT_DetectorManager* m_detManager {nullptr};

    Gaudi::Property<int> m_minNumberMeasurements {this, "MinNumberMeasurements", 15};
    Gaudi::Property<int> m_maxTrackChi2{this, "MaxTrackChi2", 200};
    Gaudi::Property<bool> m_resolvePassive {this, "resolvePassive", false};
    Gaudi::Property<bool> m_resolveMaterial {this, "resolveMaterial", true};
    Gaudi::Property<bool> m_resolveSensitive {this, "resolveSensitive", true};
    SG::ReadHandleKey<TrackCollection> m_trackCollection { this, "TrackCollection", "CKFTrackCollection", "Input track collection name" };
    SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};
    ToolHandle<IFaserActsTrackingGeometryTool> m_trackingGeometryTool {this, "TrackingGeometryTool", "FaserActsTrackingGeometryTool"};
    ToolHandle<IFaserActsExtrapolationTool> m_extrapolationTool{this, "ExtrapolationTool", "FaserActsExtrapolationTool"};

const Acts::BoundTrackParameters ATLASTrackParameterToActs(const Trk::TrackParameters *atlasParameter) const ;
    double getDerivation(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2, int global)const ;
    double getDerivationOnParam(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, int ia, Amg::Transform3D trans2)const ;
    double getLocalDerivation(Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2)const;
    //output ntuple
    ServiceHandle<ITHistSvc>  m_thistSvc;
    TTree* m_tree;
    mutable int m_eventId=0;
    mutable std::vector<double> m_fitParam_x;
    mutable std::vector<double> m_fitParam_charge;
    mutable std::vector<double> m_fitParam_y;
    mutable std::vector<double> m_fitParam_z;
    mutable std::vector<double> m_fitParam_px;
    mutable std::vector<double> m_fitParam_py;
    mutable std::vector<double> m_fitParam_pz;
    mutable std::vector<double> m_fitParam_chi2;
    mutable std::vector<int> m_fitParam_ndf;
    mutable std::vector<int> m_fitParam_nHoles;
    mutable std::vector<int> m_fitParam_nOutliers;
    mutable std::vector<int> m_fitParam_nStates;
    mutable std::vector<int> m_fitParam_nMeasurements;
    mutable std::vector<std::vector<double>> m_align_id;
    mutable std::vector<std::vector<double>> m_align_global_measured_x;
    mutable std::vector<std::vector<double>> m_align_global_measured_y;
    mutable std::vector<std::vector<double>> m_align_global_measured_z;
    mutable std::vector<std::vector<double>> m_align_global_measured_ye;
    mutable std::vector<std::vector<double>> m_align_global_fitted_ye;
    mutable std::vector<std::vector<double>> m_align_global_fitted_y;
    mutable std::vector<std::vector<double>> m_align_global_fitted_x;
    mutable std::vector<std::vector<double>> m_align_local_residual_x;
    mutable std::vector<std::vector<double>> m_align_local_measured_x;
    mutable std::vector<std::vector<double>> m_align_local_measured_xe;
    mutable std::vector<std::vector<double>> m_align_local_fitted_xe;
    mutable std::vector<std::vector<double>> m_align_local_fitted_x;
    mutable std::vector<std::vector<double>> m_align_derivation_x_x;
    mutable std::vector<std::vector<double>> m_align_derivation_x_y;
    mutable std::vector<std::vector<double>> m_align_derivation_x_z;
    mutable std::vector<std::vector<double>> m_align_derivation_x_rx;
    mutable std::vector<std::vector<double>> m_align_derivation_x_ry;
    mutable std::vector<std::vector<double>> m_align_derivation_x_rz;
    mutable std::vector<std::vector<double>> m_align_derivation_x_par_x;
    mutable std::vector<std::vector<double>> m_align_derivation_x_par_y;
    mutable std::vector<std::vector<double>> m_align_derivation_x_par_theta;
    mutable std::vector<std::vector<double>> m_align_derivation_x_par_phi;
    mutable std::vector<std::vector<double>> m_align_derivation_x_par_qop;
    mutable std::vector<std::vector<double>> m_align_derivation_global_y_x;
    mutable std::vector<std::vector<double>> m_align_derivation_global_y_y;
    mutable std::vector<std::vector<double>> m_align_derivation_global_y_z;
    mutable std::vector<std::vector<double>> m_align_derivation_global_y_rx;
    mutable std::vector<std::vector<double>> m_align_derivation_global_y_ry;
    mutable std::vector<std::vector<double>> m_align_derivation_global_y_rz;
};

#endif // FASERACTSKALMANFILTER_TrackerGlobalAlignment_H

