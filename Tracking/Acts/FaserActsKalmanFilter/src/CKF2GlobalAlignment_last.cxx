#include "CKF2GlobalAlignment.h"

#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointCollection.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrkPrepRawData/PrepRawData.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "TrkRIO_OnTrack/RIO_OnTrack.h"
#include "TrkSurfaces/Surface.h"
#include "Identifier/Identifier.h"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsRecMultiTrajectory.h"
#include "TrackSelection.h"
#include <algorithm>

#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"

#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Navigator.hpp"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/Surfaces/PlaneSurface.hpp"
#include "Acts/Surfaces/RectangleBounds.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/TrackFitting/GainMatrixSmoother.hpp"
#include "Acts/TrackFitting/GainMatrixUpdater.hpp"


#include "Acts/EventData/Measurement.hpp"
#include "Acts/Definitions/Units.hpp"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TMatrixDSym.h"
#include "TMatrixD.h"
#include <fstream>
#include <iostream>

size_t CKF2GlobalAlignment::TrajectoryInfo::nClusters {0};

using TrajectoriesContainer = std::vector<FaserActsRecMultiTrajectory>;

using namespace Acts::UnitLiterals;

CKF2GlobalAlignment::CKF2GlobalAlignment(
    const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator), m_thistSvc("THistSvc", name) {}


  StatusCode CKF2GlobalAlignment::initialize() {
    ATH_CHECK(m_fieldCondObjInputKey.initialize());
    ATH_CHECK(m_trackingGeometryTool.retrieve());
    ATH_CHECK(m_extrapolationTool.retrieve());
    ATH_CHECK(m_clusterContainerKey.initialize());
    ATH_CHECK(m_trackSeedTool.retrieve());
    ATH_CHECK(m_kalmanFitterTool1.retrieve());
    ATH_CHECK(m_createTrkTrackTool.retrieve());
    ATH_CHECK(m_spacePointContainerKey.initialize());
    ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
    m_fit = makeTrackFinderFunction(m_trackingGeometryTool->trackingGeometry(),
	m_resolvePassive, m_resolveMaterial, m_resolveSensitive);
    m_kf = makeTrackFitterFunction(m_trackingGeometryTool->trackingGeometry());
    // FIXME fix Acts logging level
    if (m_actsLogging == "VERBOSE") {
      m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::VERBOSE);
    } else if (m_actsLogging == "DEBUG") {
      m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::DEBUG);
    } else {
      m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::INFO);
    }
    CHECK(m_thistSvc.retrieve());
    m_tree= new TTree("trackParam","tree");
    initializeTree();
    CHECK(m_thistSvc->regTree("/CKF2GlobalAlignment/trackParam",m_tree));
    m_tree->AutoSave();
    return StatusCode::SUCCESS;
  }


StatusCode CKF2GlobalAlignment::execute() {
  clearVariables();
  const EventContext& ctx = Gaudi::Hive::currentContext();
  m_runId= ctx.eventID().run_number();
  m_eventId= ctx.eventID().event_number();
  m_eventtime = ctx.eventID().time_stamp();
  m_bcid = ctx.eventID().bunch_crossing_id();

  m_numberOfEvents++;

  ATH_MSG_DEBUG("get the tracking geometry");
  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
    = m_trackingGeometryTool->trackingGeometry();

  const FaserActsGeometryContext& faserActsGeometryContext = m_trackingGeometryTool->getGeometryContext();
  auto gctx = faserActsGeometryContext.context();
  Acts::MagneticFieldContext magFieldContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext;
  ATH_MSG_DEBUG("get the seed");

  CHECK(m_trackSeedTool->run());
  std::shared_ptr<const Acts::Surface> initialSurface =
    m_trackSeedTool->initialSurface();
  std::shared_ptr<std::vector<Acts::CurvilinearTrackParameters>> initialParameters =
    m_trackSeedTool->initialTrackParameters();
  std::shared_ptr<std::vector<IndexSourceLink>> sourceLinks =
    m_trackSeedTool->sourceLinks();
//  double origin = m_trackSeedTool->targetZPosition();

  ATH_MSG_DEBUG("get the output of seeding");
  std::shared_ptr<std::vector<Measurement>> measurements = m_trackSeedTool->measurements();
  std::shared_ptr<std::vector<const Tracker::FaserSCT_Cluster*>> clusters = m_trackSeedTool->clusters();
  std::vector<const Tracker::FaserSCT_Cluster*> clusters_sta0;
  clusters_sta0.clear();
  std::shared_ptr<std::vector<const Tracker::FaserSCT_SpacePoint*>> spacePoints = m_trackSeedTool->spacePoints();
  std::shared_ptr<std::vector<std::array<std::vector<const Tracker::FaserSCT_Cluster*>, 3>>> seedClusters = m_trackSeedTool->seedClusters();

  TrajectoryInfo::nClusters = sourceLinks->size();
  TrajectoriesContainer trajectories;
  trajectories.reserve(initialParameters->size());
  if(sourceLinks->size()<15||sourceLinks->size()>40)return StatusCode::SUCCESS;
  ATH_MSG_DEBUG("size of clusters/sourcelinks "<<clusters->size()<<"/"<<sourceLinks->size());

  Acts::PropagatorPlainOptions pOptions;
  pOptions.maxSteps = m_maxSteps;

  Acts::MeasurementSelector::Config measurementSelectorCfg = {
    {Acts::GeometryIdentifier(), {m_chi2Max, m_nMax}},
  };


  // Set the CombinatorialKalmanFilter options
  CKF2GlobalAlignment::TrackFinderOptions options(
      gctx, magFieldContext, calibContext,
      IndexSourceLinkAccessor(), MeasurementCalibrator(*measurements),
      Acts::MeasurementSelector(measurementSelectorCfg),
      Acts::LoggerWrapper{*m_logger}, pOptions, &(*initialSurface));

  // Perform the track finding for all initial parameters
  m_numberOfTrackSeeds += initialParameters->size();
  ATH_MSG_DEBUG("Invoke track finding with " << initialParameters->size() << " seeds.");
  IndexSourceLinkContainer tmp;
  for (const auto& sl : *sourceLinks) {
    tmp.emplace_hint(tmp.end(), sl);
  }

  for (const auto& init : *initialParameters) {
    ATH_MSG_DEBUG("  position: " << init.position(gctx).transpose());
    ATH_MSG_DEBUG("  momentum: " << init.momentum().transpose());
    ATH_MSG_DEBUG("  charge:   " << init.charge());
  }

  ATH_MSG_DEBUG("sourcelink size "<<tmp.size());
  auto results = (*m_fit)(tmp, *initialParameters, options);

  // results contain a MultiTrajectory for each track seed with a trajectory of each branch of the CKF.
  // To simplify the ambiguity solving a list of MultiTrajectories is created, each containing only a single track.
  std::list<TrajectoryInfo> allTrajectories;
  for (auto &result : results) {
    if (not result.ok()) {
      continue;
    }
    CKFResult ckfResult = result.value();
    for (size_t trackTip : ckfResult.lastMeasurementIndices) {
      allTrajectories.emplace_back(TrajectoryInfo(FaserActsRecMultiTrajectory(
	      ckfResult.fittedStates, {trackTip}, {{trackTip, ckfResult.fittedParameters.at(trackTip)}})));
    }
  }
  m_numberOfFittedTracks += allTrajectories.size();

  // the list of MultiTrajectories is sorted by the number of measurements using the chi2 value as a tie-breaker
  allTrajectories.sort([](const TrajectoryInfo &left, const TrajectoryInfo &right) {
      if (left.nMeasurements > right.nMeasurements) return true;
      if (left.nMeasurements < right.nMeasurements) return false;
      if (left.chi2 < right.chi2) return true;
      else return false;
      });

  // select all tracks with at least 13 heats and with 6 or less shared hits, starting from the best track
  // TODO use Gaudi parameters for the number of hits and shared hits
  // TODO allow shared hits only in the first station?
  std::vector<FaserActsRecMultiTrajectory> selectedTrajectories {};
  while (not allTrajectories.empty()) {
    TrajectoryInfo selected = allTrajectories.front();
    selectedTrajectories.push_back(selected.trajectory);
    allTrajectories.remove_if([&](const TrajectoryInfo &p) {
	return (p.nMeasurements <= 12) || ((p.clusterSet & selected.clusterSet).count() > 6);
	});
  }

  bool save=false;
  //only use the first track
  int trackid=0;
  for (const FaserActsRecMultiTrajectory &traj : selectedTrajectories) {
    if(trackid>0)continue;
    trackid++;
    const auto& mj = traj.multiTrajectory();
    const auto& trackTips = traj.tips();
    if(trackTips.empty())continue;
    auto& trackTip = trackTips.front();
    auto trajState = Acts::MultiTrajectoryHelpers::trajectoryState(mj, trackTip);
    if(trajState.nMeasurements<15||trajState.chi2Sum>200)continue;
    std::unique_ptr<Trk::Track> track = m_createTrkTrackTool->createTrack(gctx, traj);
    if (track==nullptr) continue;

    const auto params = traj.trackParameters(traj.tips().front());
    ATH_MSG_DEBUG("Fitted parameters");
    ATH_MSG_DEBUG("  params:   " << params.parameters().transpose());
    ATH_MSG_DEBUG("  position: " << params.position(gctx).transpose());
    ATH_MSG_DEBUG("  momentum: " << params.momentum().transpose());
    ATH_MSG_DEBUG("  charge:   " << params.charge());
    ATH_MSG_VERBOSE("checkte track");
	Acts::BoundTrackParameters* first_parameter=nullptr;
    m_fitParam_nMeasurements=trajState.nMeasurements;
    m_fitParam_nStates=trajState.nStates;
    m_fitParam_nOutliers=trajState.nOutliers;
    m_fitParam_nHoles=trajState.nHoles;
    m_fitParam_chi2=trajState.chi2Sum;
    m_fitParam_ndf=trajState.NDF;
    m_fitParam_x=params.position(gctx).transpose().x();
    m_fitParam_y=params.position(gctx).transpose().y();
    m_fitParam_z=params.position(gctx).transpose().z();
    m_fitParam_charge=params.charge();
    m_fitParam_px=params.momentum().transpose().x();
    m_fitParam_py=params.momentum().transpose().y();
    m_fitParam_pz=params.momentum().transpose().z();
    if(params.momentum().transpose().z()<100||params.momentum().transpose().z()>5000)continue;
    //get residual from ACTS
    mj.visitBackwards(trackTip, [&](const auto &state) {
	/// Only fill the track states with non-outlier measurement
	auto typeFlags = state.typeFlags();
	if (not typeFlags.test(Acts::TrackStateFlag::MeasurementFlag) and not typeFlags.test(Acts::TrackStateFlag::OutlierFlag))
	{
	return true;
	}

	const auto& surface = state.referenceSurface();
	Acts::BoundVector meas = state.projector().transpose() * state.calibrated();
	Acts::Vector2 local(meas[Acts::eBoundLoc0], meas[Acts::eBoundLoc1]);
	const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(meas[Acts::eBoundPhi], meas[Acts::eBoundTheta]);
	Acts::Vector3 mom(1, 1, 1);
	Acts::Vector3 global = surface.localToGlobal(gctx, local, dir);
	if (state.hasSmoothed())
	{
	Acts::BoundTrackParameters parameter(
	    state.referenceSurface().getSharedPtr(),
	    state.smoothed(),
	    state.smoothedCovariance());
	auto covariance = state.smoothedCovariance();

	/// Local hit residual info
	auto H = state.effectiveProjector();
//	auto resCov = state.effectiveCalibratedCovariance() +
	  H * covariance * H.transpose();
	auto residual = state.effectiveCalibrated() - H * state.smoothed();

	if (state.hasUncalibrated()) {
	  save=true;

	  const Tracker::FaserSCT_Cluster* cluster = state.uncalibrated().hit();

	  auto trans2 = cluster->detectorElement()->surface().transform();
	  auto trans1 = trans2;
	  Identifier id = cluster->identify();
	  int istation = m_idHelper->station(id);
	  int ilayer = m_idHelper->layer(id);
	  int stereoid = m_idHelper->side(id);
	  ATH_MSG_DEBUG("  save the residual "<<residual(Acts::eBoundLoc0));
	  const auto iModuleEta = m_idHelper->eta_module(id);
	  const auto iModulePhi = m_idHelper->phi_module(id);
	  int iModule = iModulePhi;
	  if (iModuleEta < 0) iModule +=4;
	  if(stereoid==1){
	    trans1=cluster->detectorElement()->otherSide()->surface().transform();
	    m_align_id.push_back(istation*1000+ilayer*100+iModule*10+1);
	  }
	  else
	    m_align_id.push_back(istation*1000+ilayer*100+iModule*10);
	  if(istation==1&&ilayer==0)first_parameter=&parameter;
	  ATH_MSG_DEBUG("ID "<<istation<<"/"<<ilayer<<"/"<<iModule<<"/"<<stereoid<<"/"<<ilayer);
	  //get derivatives
	  m_align_global_measured_x.push_back(cluster->globalPosition().x());
	  m_align_global_measured_y.push_back(cluster->globalPosition().y());
	  m_align_global_measured_z.push_back(cluster->globalPosition().z());
	  m_align_global_fitted_x.push_back(global.x());
	  m_align_global_fitted_y.push_back(global.y());
	  m_align_local_residual_x.push_back(residual(Acts::eBoundLoc0));
	  m_align_local_measured_x.push_back(cluster->localPosition().x());
	  m_align_local_measured_xe.push_back(sqrt(cluster->localCovariance()(0,0)));
	  m_align_local_fitted_xe.push_back(sqrt(covariance(Acts::eBoundLoc0, Acts::eBoundLoc0)));
	  m_align_local_fitted_x.push_back(meas[Acts::eBoundLoc0]);
	  Amg::Vector2D local_mea=cluster->localPosition() ; 
	  //derivatives on track parameter
	  double deri_x_x=getDerivativeOnParam(ctx, params, 0,  trans2);
	  double deri_x_y=getDerivativeOnParam(ctx, params, 1,  trans2);
	  double deri_x_theta=getDerivativeOnParam(ctx, params, 2,  trans2);
	  double deri_x_phi=getDerivativeOnParam(ctx, params, 3,  trans2);
	  double deri_x_qop=getDerivativeOnParam(ctx, params, 4,  trans2);
	  if(fabs(deri_x_x-1)>2||fabs(deri_x_y)>5||fabs(deri_x_theta)>6000||fabs(deri_x_phi)>6000||fabs(deri_x_qop)>5)save=false;
	  m_align_derivative_x_par_x.push_back(deri_x_x);
	  m_align_derivative_x_par_y.push_back(deri_x_y);
	  m_align_derivative_x_par_theta.push_back(deri_x_theta);
	  m_align_derivative_x_par_phi.push_back(deri_x_phi);
	  m_align_derivative_x_par_qop.push_back(deri_x_qop);
	  //derivatives on the alignment parameter
	  m_align_derivative_x_x.push_back(getDerivative(ctx, parameter, trans1,local_mea, 0, stereoid, trans2, 0));
	  m_align_derivative_x_y.push_back(getDerivative(ctx, parameter, trans1,local_mea, 1, stereoid, trans2, 0));
	  m_align_derivative_x_z.push_back(getDerivative(ctx, parameter, trans1,local_mea, 2, stereoid, trans2, 0));
	  m_align_derivative_x_rx.push_back(getDerivative(ctx, parameter, trans1,local_mea, 3, stereoid, trans2, 0));
	  m_align_derivative_x_ry.push_back(getDerivative(ctx, parameter, trans1,local_mea, 4, stereoid, trans2, 0));
	  m_align_derivative_x_rz.push_back(getDerivative(ctx, parameter, trans1,local_mea, 5, stereoid, trans2, 0));
	  m_align_derivative_global_y_x.push_back(getDerivative(ctx, parameter, trans1,local_mea, 0, stereoid, trans2, 1));
	  m_align_derivative_global_y_y.push_back(getDerivative(ctx, parameter, trans1,local_mea, 1, stereoid, trans2, 1));
	  m_align_derivative_global_y_z.push_back(getDerivative(ctx, parameter, trans1,local_mea, 2, stereoid, trans2, 1));
	  m_align_derivative_global_y_rx.push_back(getDerivative(ctx, parameter, trans1,local_mea, 3, stereoid, trans2, 1));
	  m_align_derivative_global_y_ry.push_back(getDerivative(ctx, parameter, trans1,local_mea, 4, stereoid, trans2, 1));
	  m_align_derivative_global_y_rz.push_back(getDerivative(ctx, parameter, trans1,local_mea, 5, stereoid, trans2, 1));
	  ATH_MSG_VERBOSE("local derivative "<<m_align_derivative_x_x.back()<<" "<<m_align_derivative_x_y.back()<<" "<<m_align_derivative_x_z.back()<<" "<<m_align_derivative_x_rx.back()<<" "<<m_align_derivative_x_ry.back()<<" "<<m_align_derivative_x_rz.back());
	  ATH_MSG_VERBOSE("global derivative "<<m_align_derivative_global_y_x.back()<<" "<<m_align_derivative_global_y_y.back()<<" "<<m_align_derivative_global_y_z.back()<<""<<m_align_derivative_global_y_rx.back()<<" "<<m_align_derivative_global_y_ry.back()<<" "<<m_align_derivative_global_y_rz.back());
	}
	}
	return true;
    });

// read IFT SpacePoint
  SG::ReadHandle<FaserSCT_SpacePointContainer> spacePointContainer {m_spacePointContainerKey, ctx};
  ATH_CHECK(spacePointContainer.isValid());
  for (const FaserSCT_SpacePointCollection* spacePointCollection : *spacePointContainer) {
    for (const Tracker::FaserSCT_SpacePoint *spacePoint: *spacePointCollection) {
      Identifier id = spacePoint->cluster1()->identify();
      Identifier id2 = spacePoint->cluster2()->identify();
      int istation = m_idHelper->station(id);
      ATH_MSG_DEBUG("Read the spacePoint "<<istation<<" "<<spacePoint->localParameters());
      if(istation!=0)continue;
      int ilayer = m_idHelper->layer(id);
      const auto iModuleEta = m_idHelper->eta_module(id);
      const auto iModulePhi = m_idHelper->phi_module(id);
      int iModule = iModulePhi;
      if (iModuleEta < 0) iModule +=4;

      ATH_MSG_DEBUG("Found the IFT spacePoint "<<istation<<ilayer<<iModule<<" "<<spacePoint->globalPosition().transpose());
      if(fabs(spacePoint->globalPosition().x())>120||fabs(spacePoint->globalPosition().y())>120)continue;

      m_ift_id.push_back(istation*1000+ilayer*100+iModule*10);
      m_ift_s0_strip.push_back(m_idHelper->strip(id));
      m_ift_s1_strip.push_back(m_idHelper->strip(id2));

      m_ift_local_x.push_back(spacePoint->localParameters().x());
      m_ift_local_y.push_back(spacePoint->localParameters().y());
      m_ift_global_x.push_back(spacePoint->globalPosition().x());
      m_ift_global_y.push_back(spacePoint->globalPosition().y());
      m_ift_global_z.push_back(spacePoint->globalPosition().z());
      m_ift_local_xe.push_back(sqrt(spacePoint->localCovariance()(0,0)));
      m_ift_local_ye.push_back(sqrt(spacePoint->localCovariance()(1,1)));

      //get fitted pos
      auto ift_trans = spacePoint->associatedSurface().transform();
      auto ift_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(ift_trans, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
      std::unique_ptr<const Acts::BoundTrackParameters> ift_Param = m_extrapolationTool->propagate( ctx, params, *ift_surface, Acts::backward);

      if(ift_Param==nullptr){
        m_ift_fit_local_x.push_back(-9999);
        m_ift_fit_local_y.push_back(-9999);
        m_ift_fit_global_x.push_back(-9999);
        m_ift_fit_global_y.push_back(-9999);
        m_ift_fit_global_z.push_back(-9999);
        continue;
      }
      auto ift_parameter = ift_Param->parameters();
      m_ift_fit_global_x.push_back(ift_Param->position(gctx).transpose().x());
      m_ift_fit_global_y.push_back(ift_Param->position(gctx).transpose().y());
      m_ift_fit_global_z.push_back(ift_Param->position(gctx).transpose().z());
      m_ift_fit_local_x.push_back(ift_parameter[Acts::eBoundLoc0]);
      m_ift_fit_local_y.push_back(ift_parameter[Acts::eBoundLoc1]);
      ATH_MSG_DEBUG("Found the extrapolated track "<<ift_Param->position(gctx).transpose());
      
      m_ift_local_residual_x.push_back(spacePoint->localParameters().x() - ift_parameter[Acts::eBoundLoc0]);
      m_ift_local_residual_y.push_back(spacePoint->localParameters().y() - ift_parameter[Acts::eBoundLoc1]);

      const Acts::BoundTrackParameters& fitParameters = *ift_Param;
      //derivatives on track parameter
	    std::pair<double, double> deri_x=IFTgetDerivativeOnParam(ctx, params, 0, ift_trans);
	    std::pair<double, double> deri_y=IFTgetDerivativeOnParam(ctx, params, 1, ift_trans);
	    std::pair<double, double> deri_theta=IFTgetDerivativeOnParam(ctx, params, 2, ift_trans);
	    std::pair<double, double> deri_phi=IFTgetDerivativeOnParam(ctx, params, 3, ift_trans);
	    std::pair<double, double> deri_qop=IFTgetDerivativeOnParam(ctx, params, 4, ift_trans);
	    if(fabs(deri_x.first-1)>2||fabs(deri_y.first)>5||fabs(deri_theta.first)>6000||fabs(deri_phi.first)>6000||fabs(deri_qop.first)>5)save=false;
	    m_ift_align_derivative_x_par_x.push_back(deri_x.first);
	    m_ift_align_derivative_x_par_y.push_back(deri_y.first);
	    m_ift_align_derivative_x_par_theta.push_back(deri_theta.first);
	    m_ift_align_derivative_x_par_phi.push_back(deri_phi.first);
	    m_ift_align_derivative_x_par_qop.push_back(deri_qop.first);
      m_ift_align_derivative_y_par_x.push_back(deri_x.second);
	    m_ift_align_derivative_y_par_y.push_back(deri_y.second);
	    m_ift_align_derivative_y_par_theta.push_back(deri_theta.second);
	    m_ift_align_derivative_y_par_phi.push_back(deri_phi.second);
	    m_ift_align_derivative_y_par_qop.push_back(deri_qop.second);

      //derivatives on the alignment parameter
      Amg::Vector2D ift_local_mea = spacePoint->localParameters() ; 
      std::pair<double, double> local_deri_x   = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 0, 0);
      std::pair<double, double> local_deri_y   = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 1, 0);
      std::pair<double, double> local_deri_z   = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 2, 0);
      std::pair<double, double> local_deri_rx  = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 3, 0);
      std::pair<double, double> local_deri_ry  = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 4, 0);
      std::pair<double, double> local_deri_rz  = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 5, 0);
      std::pair<double, double> global_deri_x  = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 0, 1);
      std::pair<double, double> global_deri_y  = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 1, 1);
      std::pair<double, double> global_deri_z  = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 2, 1);
      std::pair<double, double> global_deri_rx = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 3, 1);
      std::pair<double, double> global_deri_ry = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 4, 1);
      std::pair<double, double> global_deri_rz = IFTgetDerivative(ctx, fitParameters, ift_trans, ift_local_mea, 5, 1);
      m_ift_align_derivative_x_x.push_back(local_deri_x.first);
      m_ift_align_derivative_x_y.push_back(local_deri_y.first);
      m_ift_align_derivative_x_z.push_back(local_deri_z.first);
      m_ift_align_derivative_x_rx.push_back(local_deri_rx.first);
      m_ift_align_derivative_x_ry.push_back(local_deri_ry.first);
      m_ift_align_derivative_x_rz.push_back(local_deri_rz.first);
      m_ift_align_derivative_y_x.push_back(local_deri_x.second);
      m_ift_align_derivative_y_y.push_back(local_deri_y.second);
      m_ift_align_derivative_y_z.push_back(local_deri_z.second);
      m_ift_align_derivative_y_rx.push_back(local_deri_rx.second);
      m_ift_align_derivative_y_ry.push_back(local_deri_ry.second);
      m_ift_align_derivative_y_rz.push_back(local_deri_rz.second);
      m_ift_align_derivative_global_y_x.push_back(global_deri_x.first);
      m_ift_align_derivative_global_y_y.push_back(global_deri_y.first);
      m_ift_align_derivative_global_y_z.push_back(global_deri_z.first);
      m_ift_align_derivative_global_y_rx.push_back(global_deri_rx.first);
      m_ift_align_derivative_global_y_ry.push_back(global_deri_ry.first);
      m_ift_align_derivative_global_y_rz.push_back(global_deri_rz.first);
      m_ift_align_derivative_global_x_x.push_back(global_deri_x.second);
      m_ift_align_derivative_global_x_y.push_back(global_deri_y.second);
      m_ift_align_derivative_global_x_z.push_back(global_deri_z.second);
      m_ift_align_derivative_global_x_rx.push_back(global_deri_rx.second);
      m_ift_align_derivative_global_x_ry.push_back(global_deri_ry.second);
      m_ift_align_derivative_global_x_rz.push_back(global_deri_rz.second);

      } // space point container loop
    } // space point collection loop


    //read cluster for IFT
  SG::ReadHandle<Tracker::FaserSCT_ClusterContainer> clusterContainer {m_clusterContainerKey};
  ATH_CHECK(clusterContainer.isValid());
  for (const Tracker::FaserSCT_ClusterCollection* clusterCollection : *clusterContainer) {
      for (const Tracker::FaserSCT_Cluster* cluster : *clusterCollection) {
	Identifier id = cluster->identify();
	int istation = m_idHelper->station(id);
	ATH_MSG_DEBUG("Read the cluster "<<istation<<" "<<cluster->localPosition());
	if(istation!=0)continue;
	int ilayer = m_idHelper->layer(id);
	int stereoid = m_idHelper->side(id);
	if(stereoid!=1)stereoid=0;
	const auto iModuleEta = m_idHelper->eta_module(id);
	const auto iModulePhi = m_idHelper->phi_module(id);
	int iModule = iModulePhi;
	if (iModuleEta < 0) iModule +=4;
	m_ift_clus_id.push_back(istation*1000+ilayer*100+iModule*10+stereoid);
	m_ift_clus_strip.push_back(m_idHelper->strip(id));
	ATH_MSG_DEBUG("Found the IFT cluster "<<istation<<ilayer<<iModule<<stereoid<<" "<<cluster->globalPosition().transpose());
	if(fabs(cluster->globalPosition().x())>120||fabs(cluster->globalPosition().y())>120)continue;

	m_ift_clus_local_x.push_back(cluster->localPosition().x());
	m_ift_clus_local_y.push_back(cluster->localPosition().y());
	m_ift_clus_global_x.push_back(cluster->globalPosition().x());
	m_ift_clus_global_y.push_back(cluster->globalPosition().y());
	m_ift_clus_global_z.push_back(cluster->globalPosition().z());
	//get fitted pos
	auto ift_trans= cluster->detectorElement()->surface().transform();
	auto ift_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(ift_trans, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
	std::unique_ptr<const Acts::BoundTrackParameters> ift_Param = m_extrapolationTool->propagate( ctx, params, *ift_surface, Acts::backward);
//	if(first_parameter==nullptr)continue;
	//std::unique_ptr<const Acts::BoundTrackParameters> ift_Param = m_extrapolationTool->propagate( ctx, *first_parameter, *ift_surface, Acts::backward);
	if(ift_Param==nullptr){
	  m_ift_clus_fit_local_x.push_back(-9999);
	  m_ift_clus_fit_local_y.push_back(-9999);
	  m_ift_clus_fit_global_x.push_back(-9999);
	  m_ift_clus_fit_global_y.push_back(-9999);
	  m_ift_clus_fit_global_z.push_back(-9999);
	  continue;
	}
	auto ift_parameter = ift_Param->parameters();
	m_ift_clus_fit_global_x.push_back(ift_Param->position(gctx).transpose().x());
	m_ift_clus_fit_global_y.push_back(ift_Param->position(gctx).transpose().y());
	m_ift_clus_fit_global_z.push_back(ift_Param->position(gctx).transpose().z());
	m_ift_clus_fit_local_x.push_back(ift_parameter[Acts::eBoundLoc0]);
	m_ift_clus_fit_local_y.push_back(ift_parameter[Acts::eBoundLoc1]);
	ATH_MSG_DEBUG("Found the extrapolated track "<<ift_Param->position(gctx).transpose());
      }
    }



  } //track loop
  ATH_MSG_VERBOSE("finish getting residuals");


  if(save)
    m_tree->Fill();

  return StatusCode::SUCCESS;
}


StatusCode CKF2GlobalAlignment::finalize() {
  ATH_MSG_INFO("CombinatorialKalmanFilterAlg::finalize()");
  ATH_MSG_INFO(m_numberOfEvents << " events processed.");
  ATH_MSG_INFO(m_numberOfTrackSeeds << " seeds.");
  ATH_MSG_INFO(m_numberOfFittedTracks << " fitted tracks.");
  ATH_MSG_INFO(m_numberOfSelectedTracks << " selected and re-fitted tracks.");
  /*
  //dump the matrix to a txt file for alignment
  std::vector<double>* t_fitParam_chi2=0;
  std::vector<double>* t_fitParam_pz=0;
  std::vector<double>* t_fitParam_x=0;
  std::vector<double>* t_fitParam_y=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_residual_x_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_measured_x_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_measured_xe_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_stationId_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_centery_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_layerId_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_moduleId_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_unbiased_sp=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivative_x_x=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivative_x_y=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivative_x_z=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivative_x_rx=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivative_x_ry=0;
  std::vector<std::vector<double>>* t_fitParam_align_local_derivative_x_rz=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivative_y_x=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivative_y_y=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivative_y_z=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivative_y_rx=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivative_y_ry=0;
  std::vector<std::vector<double>>* t_fitParam_align_global_derivative_y_rz=0;
  std::vector<std::vector<double>>* t_fitParam_align_stereoId=0;
  std::vector<int>* t_fitParam_nMeasurements=0;
  m_tree->SetBranchAddress("fitParam_nMeasurements", &t_fitParam_nMeasurements);
  m_tree->SetBranchAddress("fitParam_align_stereoId",&t_fitParam_align_stereoId);
  m_tree->SetBranchAddress("fitParam_align_local_derivative_x_x",&t_fitParam_align_local_derivative_x_x);
  m_tree->SetBranchAddress("fitParam_align_local_derivative_x_y",&t_fitParam_align_local_derivative_x_y);
  m_tree->SetBranchAddress("fitParam_align_local_derivative_x_z",&t_fitParam_align_local_derivative_x_z);
  m_tree->SetBranchAddress("fitParam_align_local_derivative_x_rx",&t_fitParam_align_local_derivative_x_rx);
  m_tree->SetBranchAddress("fitParam_align_local_derivative_x_ry",&t_fitParam_align_local_derivative_x_ry);
  m_tree->SetBranchAddress("fitParam_align_local_derivative_x_rz",&t_fitParam_align_local_derivative_x_rz);
  m_tree->SetBranchAddress("fitParam_align_global_derivative_y_x",&t_fitParam_align_global_derivative_y_x);
  m_tree->SetBranchAddress("fitParam_align_global_derivative_y_y",&t_fitParam_align_global_derivative_y_y);
  m_tree->SetBranchAddress("fitParam_align_global_derivative_y_z",&t_fitParam_align_global_derivative_y_z);
  m_tree->SetBranchAddress("fitParam_align_global_derivative_y_rx",&t_fitParam_align_global_derivative_y_rx);
  m_tree->SetBranchAddress("fitParam_align_global_derivative_y_ry",&t_fitParam_align_global_derivative_y_ry);
  m_tree->SetBranchAddress("fitParam_align_global_derivative_y_rz",&t_fitParam_align_global_derivative_y_rz);
  m_tree->SetBranchAddress("fitParam_align_local_residual_x_sp",&t_fitParam_align_local_residual_x_sp);
  m_tree->SetBranchAddress("fitParam_chi2",&t_fitParam_chi2);
  m_tree->SetBranchAddress("fitParam_pz",&t_fitParam_pz);
  m_tree->SetBranchAddress("fitParam_x",&t_fitParam_x);
  m_tree->SetBranchAddress("fitParam_y",&t_fitParam_y);
  m_tree->SetBranchAddress("fitParam_align_local_measured_x_sp",&t_fitParam_align_local_measured_x_sp);
  m_tree->SetBranchAddress("fitParam_align_local_measured_xe_sp",&t_fitParam_align_local_measured_xe_sp);
  m_tree->SetBranchAddress("fitParam_align_layerId_sp",&t_fitParam_align_layerId_sp);
  m_tree->SetBranchAddress("fitParam_align_moduleId_sp",&t_fitParam_align_moduleId_sp);
  m_tree->SetBranchAddress("fitParam_align_unbiased_sp",&t_fitParam_align_unbiased_sp);
  m_tree->SetBranchAddress("fitParam_align_stationId_sp",&t_fitParam_align_stationId_sp);
  m_tree->SetBranchAddress("fitParam_align_centery_sp",&t_fitParam_align_centery_sp);
  int ntrk_mod[12][8]={0};
  int ntrk_sta[4]={0};
  int ntrk_lay[12]={0};
  //define matrix
  int Ndim=6;
  std::vector<TMatrixD> denom_lay;
  std::vector<TMatrixD> num_lay;
  std::vector<std::vector<TMatrixD>> denom_mod;
  std::vector<std::vector<TMatrixD>> num_mod;
  std::vector<std::vector<TMatrixD>> denom_mod1;
  std::vector<std::vector<TMatrixD>> num_mod1;
  //initialize to 0
  auto num_matrix = [](int n) {
    TMatrixD num_t(n,1);
    for(int i=0;i<n;i++){
      num_t[i][0]=0.;
    }
    return num_t;
  };
  auto denom_matrix = [](int n) {
    TMatrixD denom_t(n,n);
    for(int i=0;i<n;i++){
      for(int j=0;j<n;j++){
	denom_t[i][j]=0.;
      }
    }
    return denom_t;
  };
  std::vector<TMatrixD> denom_sta;
  std::vector<TMatrixD> num_sta;
  for(int i=0;i<4;i++){
    denom_sta.push_back(denom_matrix(Ndim));
    num_sta.push_back(num_matrix(Ndim));
  }
  for(int i=0;i<12;i++){
    denom_lay.push_back(denom_matrix(Ndim));
    num_lay.push_back(num_matrix(Ndim));
    std::vector<TMatrixD> denom_l;
    std::vector<TMatrixD> num_l;
    std::vector<TMatrixD> denom_l1;
    std::vector<TMatrixD> num_l1;
    for(int j=0;j<8;j++){
      denom_l.push_back(denom_matrix(Ndim));
      num_l.push_back(num_matrix(Ndim));
      denom_l1.push_back(denom_matrix(Ndim));
      num_l1.push_back(num_matrix(Ndim));
    }
    denom_mod.push_back(denom_l1);
    denom_mod1.push_back(denom_l1);
    num_mod.push_back(num_l);
    num_mod1.push_back(num_l1);
  }
  double chi_mod_y[12][8]={0};
  std::cout<<"found "<<m_tree->GetEntries()<<" events "<<std::endl;
  //loop over all the entries
  for(int ievt=0;ievt<m_tree->GetEntries();ievt++){
    m_tree->GetEntry(ievt);
    if(ievt%10000==0)std::cout<<"processing "<<ievt<<" event"<<std::endl;
    if(t_fitParam_chi2->size()<1)continue;
    for(int i=0;i<1;i+=1){
      if(t_fitParam_chi2->at(i)>100||t_fitParam_pz->at(i)<300||sqrt(t_fitParam_x->at(i)*t_fitParam_x->at(i)+t_fitParam_y->at(i)*t_fitParam_y->at(i))>95)continue;
      if(t_fitParam_align_local_residual_x_sp->at(i).size()<15)continue;//only     use good track
      for(size_t j=0;j<t_fitParam_align_local_residual_x_sp->at(i).size();j++){
	double resx1=999.;
	double x1e=999.;
	if(m_biased&&t_fitParam_align_stationId_sp->at(i).at(j)==0){
	  if(t_fitParam_align_unbiased_sp->at(i).at(j)==1){
	    resx1=(t_fitParam_align_local_residual_x_sp->at(i).at(j));
	    x1e=sqrt(t_fitParam_align_local_measured_xe_sp->at(i).at(j));
	    if(fabs(resx1)>1.0)continue;
	  }
	  else continue;
	}
	else{
	  resx1=t_fitParam_align_local_residual_x_sp->at(i).at(j);
	  x1e=sqrt(t_fitParam_align_local_measured_xe_sp->at(i).at(j));
	  if(fabs(resx1)>0.2)continue;
	}
	TMatrixD m1(Ndim,1);
	m1[0][0]=t_fitParam_align_local_derivative_x_x->at(i).at(j)/x1e;
	m1[1][0]=t_fitParam_align_local_derivative_x_y->at(i).at(j)/x1e;
	m1[2][0]=t_fitParam_align_local_derivative_x_z->at(i).at(j)/x1e;
	m1[3][0]=t_fitParam_align_local_derivative_x_rx->at(i).at(j)/x1e;
	m1[4][0]=t_fitParam_align_local_derivative_x_ry->at(i).at(j)/x1e;
	m1[5][0]=t_fitParam_align_local_derivative_x_rz->at(i).at(j)/x1e;
	TMatrixD m2(1,Ndim);
	TMatrixD m3(Ndim,Ndim);
	m2.Transpose(m1);
	m3=m1*m2;
	TMatrixD m4(Ndim,1);
	m4[0][0]=t_fitParam_align_local_derivative_x_x->at(i).at(j)/x1e*resx1/x1e;
	m4[1][0]=t_fitParam_align_local_derivative_x_y->at(i).at(j)/x1e*resx1/x1e;
	m4[2][0]=t_fitParam_align_local_derivative_x_z->at(i).at(j)/x1e*resx1/x1e;
	m4[3][0]=t_fitParam_align_local_derivative_x_rx->at(i).at(j)/x1e*resx1/x1e;
	m4[4][0]=t_fitParam_align_local_derivative_x_ry->at(i).at(j)/x1e*resx1/x1e;
	m4[5][0]=t_fitParam_align_local_derivative_x_rz->at(i).at(j)/x1e*resx1/x1e;
	TMatrixD m6(Ndim,1);
	m6=m4;
	TMatrixD mg1(Ndim,1);
	mg1[0][0]=t_fitParam_align_global_derivative_y_x->at(i).at(j)/x1e;
	mg1[1][0]=t_fitParam_align_global_derivative_y_y->at(i).at(j)/x1e;
	mg1[2][0]=t_fitParam_align_global_derivative_y_z->at(i).at(j)/x1e;
	mg1[3][0]=t_fitParam_align_global_derivative_y_rx->at(i).at(j)/x1e;
	mg1[4][0]=t_fitParam_align_global_derivative_y_ry->at(i).at(j)/x1e;
	mg1[5][0]=t_fitParam_align_global_derivative_y_rz->at(i).at(j)/x1e;
	TMatrixD mg2(1,Ndim);
	TMatrixD mg3(Ndim,Ndim);
	mg2.Transpose(mg1);
	mg3=mg1*mg2;
	TMatrixD mg4(Ndim,1);
	mg4[0][0]=t_fitParam_align_global_derivative_y_x->at(i).at(j)/x1e*resx1/x1e;
	mg4[1][0]=t_fitParam_align_global_derivative_y_y->at(i).at(j)/x1e*resx1/x1e;
	mg4[2][0]=t_fitParam_align_global_derivative_y_z->at(i).at(j)/x1e*resx1/x1e;
	mg4[3][0]=t_fitParam_align_global_derivative_y_rx->at(i).at(j)/x1e*resx1/x1e;
	mg4[4][0]=t_fitParam_align_global_derivative_y_ry->at(i).at(j)/x1e*resx1/x1e;
	mg4[5][0]=t_fitParam_align_global_derivative_y_rz->at(i).at(j)/x1e*resx1/x1e;
	TMatrixD mg6(Ndim,1);
	mg6=mg4;
	int istation=t_fitParam_align_stationId_sp->at(i).at(j);
	int ilayer=t_fitParam_align_layerId_sp->at(i).at(j);
	int imodule=t_fitParam_align_moduleId_sp->at(i).at(j);
	//station: 1,2,3
	if(istation>=0&&istation<4){
	  denom_sta[istation]+=mg3;
	  num_sta[istation]+=mg6;
	  ntrk_sta[istation]+=1;
	}
	int ilayer_tot = ilayer + (istation )*3;
	chi_mod_y[ilayer_tot][imodule]+=0;
	if(ilayer_tot>=0&&ilayer_tot<12){
	  denom_lay[ilayer_tot]+=mg3;
	  num_lay[ilayer_tot]+=mg6;
	  ntrk_lay[ilayer_tot]+=1;
	  denom_mod[ilayer_tot][imodule]+=m3;
	  num_mod[ilayer_tot][imodule]+=m6;
	  ntrk_mod[ilayer_tot][imodule]+=1;
	}
      }
    }
  }

  std::cout<<"Get the nominal transform"<<std::endl;
  //get the alignment constants
  std::ofstream align_output("alignment_matrix.txt",std::ios::out);
  for(int i=0;i<4;i++){
    for(int ii=0;ii<Ndim;ii++){
      align_output<<i<<" "<<ii<<" "<<denom_sta[i][ii][0]<<" "<<denom_sta[i][ii][1]<<" "<<denom_sta[i][ii][2]<<" "<<denom_sta[i][ii][3]<<" "<<denom_sta[i][ii][4]<<" "<<denom_sta[i][ii][5]<<" "<<0<<std::endl;
      if(ii==0)
	std::cout<<i<<" "<<ii<<" "<<denom_sta[i][ii][0]<<" "<<denom_sta[i][ii][1]<<" "<<denom_sta[i][ii][2]<<" "<<denom_sta[i][ii][3]<<" "<<denom_sta[i][ii][4]<<" "<<denom_sta[i][ii][5]<<" "<<0<<std::endl;
    }
    align_output<<i<<" "<<6<<" "<<num_sta[i][0][0]<<" "<<num_sta[i][1][0]<<" "<<num_sta[i][2][0]<<" "<<num_sta[i][3][0]<<" "<<num_sta[i][4][0]<<" "<<num_sta[i][5][0]<<" "<<ntrk_sta[i]<<std::endl;
  }
  std::cout<<"Get the deltas for stations"<<std::endl;
  //layers
  for(size_t i=0;i<denom_lay.size();i++){
    for(int ii=0;ii<Ndim;ii++){
      align_output<<i/3<<i%3<<" "<<ii<<" "<<denom_lay[i][ii][0]<<" "<<denom_lay[i][ii][1]<<" "<<denom_lay[i][ii][2]<<" "<<denom_lay[i][ii][3]<<" "<<denom_lay[i][ii][4]<<" "<<denom_lay[i][ii][5]<<" "<<0<<std::endl;
    }
    align_output<<i/3<<i%3<<" "<<6<<" "<<num_lay[i][0][0]<<" "<<num_lay[i][1][0]<<" "<<num_lay[i][2][0]<<" "<<num_lay[i][3][0]<<" "<<num_lay[i][4][0]<<" "<<num_lay[i][5][0]<<" "<<ntrk_lay[i]<<std::endl;
  }

  std::cout<<"Get the deltas for layers"<<std::endl;
  for(size_t i=0;i<denom_lay.size();i++){
    for(  size_t j=0;j<num_mod[i].size();j++){
      for(int ii=0;ii<Ndim;ii++){
	align_output<<i/3<<i%3<<j<<" "<<ii<<" "<<denom_mod[i][j][ii][0]<<" "<<denom_mod[i][j][ii][1]<<" "<<denom_mod[i][j][ii][2]<<" "<<denom_mod[i][j][ii][3]<<" "<<denom_mod[i][j][ii][4]<<" "<<denom_mod[i][j][ii][5]<<" "<<chi_mod_y[i][j]<<std::endl;
      }
      align_output<<i/3<<i%3<<j<<" "<<6<<" "<<num_mod[i][j][0][0]<<" "<<num_mod[i][j][1][0]<<" "<<num_mod[i][j][2][0]<<" "<<num_mod[i][j][3][0]<<" "<<num_mod[i][j][4][0]<<" "<<num_mod[i][j][5][0]<<" "<<ntrk_mod[i][j]<<std::endl;
    }
  }
  std::cout<<"closing the output"<<std::endl;
  align_output.close();
  */
  return StatusCode::SUCCESS;
}


Acts::MagneticFieldContext CKF2GlobalAlignment::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};
  return Acts::MagneticFieldContext(fieldCondObj);
}




void CKF2GlobalAlignment::computeSharedHits(std::vector<IndexSourceLink>* sourceLinks, TrackFinderResult& results) const {
  // Compute shared hits from all the reconstructed tracks
  // Compute nSharedhits and Update ckf results
  // hit index -> list of multi traj indexes [traj, meas]
  static_assert(Acts::SourceLinkConcept<IndexSourceLink>,
      "Source link does not fulfill SourceLinkConcept");

  std::vector<std::size_t> firstTrackOnTheHit(
      sourceLinks->size(), std::numeric_limits<std::size_t>::max());
  std::vector<std::size_t> firstStateOnTheHit(
      sourceLinks->size(), std::numeric_limits<std::size_t>::max());

  for (unsigned int iresult = 0; iresult < results.size(); iresult++) {
    if (not results.at(iresult).ok()) {
      continue;
    }

    auto& ckfResult = results.at(iresult).value();
    auto& measIndexes = ckfResult.lastMeasurementIndices;

    for (auto measIndex : measIndexes) {
      ckfResult.fittedStates.visitBackwards(measIndex, [&](const auto& state) {
	  if (not state.typeFlags().test(Acts::TrackStateFlag::MeasurementFlag))
	  return;

	  std::size_t hitIndex = state.uncalibrated().index();

	  // Check if hit not already used
	  if (firstTrackOnTheHit.at(hitIndex) ==
	      std::numeric_limits<std::size_t>::max()) {
	  firstTrackOnTheHit.at(hitIndex) = iresult;
	  firstStateOnTheHit.at(hitIndex) = state.index();
	  return;
	  }

	  // if already used, control if first track state has been marked
	  // as shared
	  int indexFirstTrack = firstTrackOnTheHit.at(hitIndex);
	  int indexFirstState = firstStateOnTheHit.at(hitIndex);
	  if (not results.at(indexFirstTrack).value().fittedStates.getTrackState(indexFirstState).typeFlags().test(Acts::TrackStateFlag::SharedHitFlag))
	  results.at(indexFirstTrack).value().fittedStates.getTrackState(indexFirstState).typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);

	  // Decorate this track
	  results.at(iresult).value().fittedStates.getTrackState(state.index()).typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);
      });
    }
  }
}


namespace {

  using Updater = Acts::GainMatrixUpdater;
  using Smoother = Acts::GainMatrixSmoother;

  using Stepper = Acts::EigenStepper<>;
  using Navigator = Acts::Navigator;
  using Propagator = Acts::Propagator<Stepper, Navigator>;
  using CKF = Acts::CombinatorialKalmanFilter<Propagator, Updater, Smoother>;

  using TrackParametersContainer = std::vector<CKF2GlobalAlignment::TrackParameters>;

  struct TrackFinderFunctionImpl
    : public CKF2GlobalAlignment::TrackFinderFunction {
      CKF trackFinder;

      TrackFinderFunctionImpl(CKF&& f) : trackFinder(std::move(f)) {}

      CKF2GlobalAlignment::TrackFinderResult operator()(
	  const IndexSourceLinkContainer& sourcelinks,
	  const TrackParametersContainer& initialParameters,
	  const CKF2GlobalAlignment::TrackFinderOptions& options)
	const override {
	  return trackFinder.findTracks(sourcelinks, initialParameters, options);
	};
    };

}  // namespace

std::shared_ptr<CKF2GlobalAlignment::TrackFinderFunction>
CKF2GlobalAlignment::makeTrackFinderFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry,
    bool resolvePassive, bool resolveMaterial, bool resolveSensitive) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  Stepper stepper(std::move(magneticField));
  Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = resolvePassive;
  cfg.resolveMaterial = resolveMaterial;
  cfg.resolveSensitive = resolveSensitive;
  Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  CKF trackFinder(std::move(propagator));

  // build the track finder functions. owns the track finder object.
  return std::make_shared<TrackFinderFunctionImpl>(std::move(trackFinder));
}


namespace {

  using Updater = Acts::GainMatrixUpdater;
  using Smoother = Acts::GainMatrixSmoother;
  using Stepper = Acts::EigenStepper<>;
  using Propagator = Acts::Propagator<Stepper, Acts::Navigator>;
  using Fitter = Acts::KalmanFitter<Propagator, Updater, Smoother>;

  struct TrackFitterFunctionImpl
    : public CKF2GlobalAlignment::TrackFitterFunction {
      Fitter trackFitter;

      TrackFitterFunctionImpl(Fitter &&f) : trackFitter(std::move(f)) {}

      CKF2GlobalAlignment::KFResult operator()(
	  const std::vector<IndexSourceLink> &sourceLinks,
	  const Acts::BoundTrackParameters &initialParameters,
	  const CKF2GlobalAlignment::TrackFitterOptions &options)
	const override {
	  return trackFitter.fit(sourceLinks, initialParameters, options);
	};
    };

}  // namespace


std::shared_ptr<CKF2GlobalAlignment::TrackFitterFunction>
CKF2GlobalAlignment::makeTrackFitterFunction(
    std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry) {
  auto magneticField = std::make_shared<FASERMagneticFieldWrapper>();
  auto stepper = Stepper(std::move(magneticField));
  Acts::Navigator::Config cfg{trackingGeometry};
  cfg.resolvePassive = false;
  cfg.resolveMaterial = true;
  cfg.resolveSensitive = true;
  Acts::Navigator navigator(cfg);
  Propagator propagator(std::move(stepper), std::move(navigator));
  Fitter trackFitter(std::move(propagator));
  return std::make_shared<TrackFitterFunctionImpl>(std::move(trackFitter));
}


void CKF2GlobalAlignment::initializeTree(){
  m_tree->Branch("evtId",&m_eventId);
  m_tree->Branch("runId",&m_runId);
  m_tree->Branch("evtTime",&m_eventtime);
  m_tree->Branch("BCID",&m_bcid);
  m_tree->Branch("fitParam_x", &m_fitParam_x);
  m_tree->Branch("fitParam_charge", &m_fitParam_charge);
  m_tree->Branch("fitParam_y", &m_fitParam_y);
  m_tree->Branch("fitParam_z", &m_fitParam_z);
  m_tree->Branch("fitParam_px", &m_fitParam_px);
  m_tree->Branch("fitParam_py", &m_fitParam_py);
  m_tree->Branch("fitParam_pz", &m_fitParam_pz);
  m_tree->Branch("fitParam_chi2", &m_fitParam_chi2);
  m_tree->Branch("fitParam_ndf", &m_fitParam_ndf);
  m_tree->Branch("fitParam_nHoles", &m_fitParam_nHoles);
  m_tree->Branch("fitParam_nOutliers", &m_fitParam_nOutliers);
  m_tree->Branch("fitParam_nStates", &m_fitParam_nStates);
  m_tree->Branch("fitParam_nMeasurements", &m_fitParam_nMeasurements);
  m_tree->Branch("ift_spaceP_id", &m_ift_id);
  m_tree->Branch("ift_spaceP_s0_strip", &m_ift_s0_strip);
  m_tree->Branch("ift_spaceP_s1_strip", &m_ift_s1_strip);
  m_tree->Branch("ift_spaceP_local_x", &m_ift_local_x);
  m_tree->Branch("ift_spaceP_local_y", &m_ift_local_y);
  m_tree->Branch("ift_spaceP_local_xe", &m_ift_local_xe);
  m_tree->Branch("ift_spaceP_local_ye", &m_ift_local_ye);
  m_tree->Branch("ift_spaceP_global_x", &m_ift_global_x);
  m_tree->Branch("ift_spaceP_global_y", &m_ift_global_y);
  m_tree->Branch("ift_spaceP_global_z", &m_ift_global_z);
  m_tree->Branch("ift_spaceP_fit_local_x", &m_ift_fit_local_x);
  m_tree->Branch("ift_spaceP_fit_local_y", &m_ift_fit_local_y);
  m_tree->Branch("ift_spaceP_fit_global_x", &m_ift_fit_global_x);
  m_tree->Branch("ift_spaceP_fit_global_y", &m_ift_fit_global_y);
  m_tree->Branch("ift_local_residual_x", &m_ift_local_residual_x);
  m_tree->Branch("ift_local_residual_y", &m_ift_local_residual_y);

  m_tree->Branch("ift_clus_id", &m_ift_clus_id);
  m_tree->Branch("ift_clus_strip", &m_ift_clus_strip);
  m_tree->Branch("ift_clus_local_x", &m_ift_clus_local_x);
  m_tree->Branch("ift_clus_local_y", &m_ift_clus_local_y);
  m_tree->Branch("ift_clus_global_x", &m_ift_clus_global_x);
  m_tree->Branch("ift_clus_global_y", &m_ift_clus_global_y);
  m_tree->Branch("ift_clus_global_z", &m_ift_clus_global_z);
  m_tree->Branch("ift_clus_fit_local_x", &m_ift_clus_fit_local_x);
  m_tree->Branch("ift_clus_fit_local_y", &m_ift_clus_fit_local_y);
  m_tree->Branch("ift_clus_fit_global_x", &m_ift_clus_fit_global_x);
  m_tree->Branch("ift_clus_fit_global_y", &m_ift_clus_fit_global_y);


  m_tree->Branch("fitParam_align_id", &m_align_id);
  m_tree->Branch("fitParam_align_local_derivative_x_par_x", &m_align_derivative_x_par_x);
  m_tree->Branch("fitParam_align_local_derivative_x_par_y", &m_align_derivative_x_par_y);
  m_tree->Branch("fitParam_align_local_derivative_x_par_theta", &m_align_derivative_x_par_theta);
  m_tree->Branch("fitParam_align_local_derivative_x_par_phi", &m_align_derivative_x_par_phi);
  m_tree->Branch("fitParam_align_local_derivative_x_par_qop", &m_align_derivative_x_par_qop);
  m_tree->Branch("fitParam_align_local_derivative_x_x", &m_align_derivative_x_x);
  m_tree->Branch("fitParam_align_local_derivative_x_y", &m_align_derivative_x_y);
  m_tree->Branch("fitParam_align_local_derivative_x_z", &m_align_derivative_x_z);
  m_tree->Branch("fitParam_align_local_derivative_x_rx", &m_align_derivative_x_rx);
  m_tree->Branch("fitParam_align_local_derivative_x_ry", &m_align_derivative_x_ry);
  m_tree->Branch("fitParam_align_local_derivative_x_rz", &m_align_derivative_x_rz);
  m_tree->Branch("fitParam_align_global_derivative_y_x", &m_align_derivative_global_y_x);
  m_tree->Branch("fitParam_align_global_derivative_y_y", &m_align_derivative_global_y_y);
  m_tree->Branch("fitParam_align_global_derivative_y_z", &m_align_derivative_global_y_z);
  m_tree->Branch("fitParam_align_global_derivative_y_rx", &m_align_derivative_global_y_rx);
  m_tree->Branch("fitParam_align_global_derivative_y_ry", &m_align_derivative_global_y_ry);
  m_tree->Branch("fitParam_align_global_derivative_y_rz", &m_align_derivative_global_y_rz);
  m_tree->Branch("fitParam_align_local_residual_x", &m_align_local_residual_x);
  m_tree->Branch("fitParam_align_local_measured_x", &m_align_local_measured_x);
  m_tree->Branch("fitParam_align_local_measured_xe", &m_align_local_measured_xe);
  m_tree->Branch("fitParam_align_local_fitted_xe", &m_align_local_fitted_xe);
  m_tree->Branch("fitParam_align_local_fitted_x", &m_align_local_fitted_x);
  m_tree->Branch("fitParam_align_global_measured_x", &m_align_global_measured_x);
  m_tree->Branch("fitParam_align_global_measured_y", &m_align_global_measured_y);
  m_tree->Branch("fitParam_align_global_measured_ye", &m_align_global_measured_ye);
  m_tree->Branch("fitParam_align_global_measured_z", &m_align_global_measured_z);
  m_tree->Branch("fitParam_align_global_fitted_ye", &m_align_global_fitted_ye);
  m_tree->Branch("fitParam_align_global_fitted_x", &m_align_global_fitted_x);
  m_tree->Branch("fitParam_align_global_fitted_y", &m_align_global_fitted_y);
  // for IFT
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_par_x",     &m_ift_align_derivative_x_par_x);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_par_y",     &m_ift_align_derivative_x_par_y);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_par_theta", &m_ift_align_derivative_x_par_theta);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_par_phi",   &m_ift_align_derivative_x_par_phi);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_par_qop",   &m_ift_align_derivative_x_par_qop);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_x",         &m_ift_align_derivative_x_x);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_y",         &m_ift_align_derivative_x_y);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_z",         &m_ift_align_derivative_x_z);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_rx",        &m_ift_align_derivative_x_rx);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_ry",        &m_ift_align_derivative_x_ry);
  m_tree->Branch("fitParam_IFT_align_local_derivative_x_rz",        &m_ift_align_derivative_x_rz);
  m_tree->Branch("fitParam_IFT_align_global_derivative_y_x",        &m_ift_align_derivative_global_y_x);
  m_tree->Branch("fitParam_IFT_align_global_derivative_y_y",        &m_ift_align_derivative_global_y_y);
  m_tree->Branch("fitParam_IFT_align_global_derivative_y_z",        &m_ift_align_derivative_global_y_z);
  m_tree->Branch("fitParam_IFT_align_global_derivative_y_rx",       &m_ift_align_derivative_global_y_rx);
  m_tree->Branch("fitParam_IFT_align_global_derivative_y_ry",       &m_ift_align_derivative_global_y_ry);
  m_tree->Branch("fitParam_IFT_align_global_derivative_y_rz",       &m_ift_align_derivative_global_y_rz);

  m_tree->Branch("fitParam_IFT_align_local_derivative_y_par_x",     &m_ift_align_derivative_y_par_x);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_par_y",     &m_ift_align_derivative_y_par_y);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_par_theta", &m_ift_align_derivative_y_par_theta);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_par_phi",   &m_ift_align_derivative_y_par_phi);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_par_qop",   &m_ift_align_derivative_y_par_qop);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_x",         &m_ift_align_derivative_y_x);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_y",         &m_ift_align_derivative_y_y);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_z",         &m_ift_align_derivative_y_z);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_rx",        &m_ift_align_derivative_y_rx);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_ry",        &m_ift_align_derivative_y_ry);
  m_tree->Branch("fitParam_IFT_align_local_derivative_y_rz",        &m_ift_align_derivative_y_rz);
  m_tree->Branch("fitParam_IFT_align_global_derivative_x_x",        &m_ift_align_derivative_global_x_x);
  m_tree->Branch("fitParam_IFT_align_global_derivative_x_y",        &m_ift_align_derivative_global_x_y);
  m_tree->Branch("fitParam_IFT_align_global_derivative_x_z",        &m_ift_align_derivative_global_x_z);
  m_tree->Branch("fitParam_IFT_align_global_derivative_x_rx",       &m_ift_align_derivative_global_x_rx);
  m_tree->Branch("fitParam_IFT_align_global_derivative_x_ry",       &m_ift_align_derivative_global_x_ry);
  m_tree->Branch("fitParam_IFT_align_global_derivative_x_rz",       &m_ift_align_derivative_global_x_rz);
}

void CKF2GlobalAlignment::clearVariables(){
  m_align_derivative_x_x.clear();
  m_align_derivative_x_y.clear();
  m_align_derivative_x_z.clear();
  m_align_derivative_x_rx.clear();
  m_align_derivative_x_ry.clear();
  m_align_derivative_x_rz.clear();
  m_align_derivative_x_par_x.clear();
  m_align_derivative_x_par_y.clear();
  m_align_derivative_x_par_theta.clear();
  m_align_derivative_x_par_phi.clear();
  m_align_derivative_x_par_qop.clear();
  m_align_derivative_global_y_x.clear();
  m_align_derivative_global_y_y.clear();
  m_align_derivative_global_y_z.clear();
  m_align_derivative_global_y_rx.clear();
  m_align_derivative_global_y_ry.clear();
  m_align_derivative_global_y_rz.clear();
  m_align_local_residual_x.clear();
  m_align_local_measured_x.clear();
  m_align_local_measured_xe.clear();
  m_align_local_fitted_xe.clear();
  m_align_local_fitted_x.clear();
  m_align_global_measured_x.clear();
  m_align_global_measured_y.clear();
  m_align_global_measured_z.clear();
  m_align_global_measured_ye.clear();
  m_align_global_fitted_ye.clear();
  m_align_global_fitted_x.clear();
  m_align_global_fitted_y.clear();
  m_align_id.clear();
  // for IFT

  m_ift_clus_id.clear();
  m_ift_clus_strip.clear();
  m_ift_clus_local_x.clear();
  m_ift_clus_local_y.clear();
  m_ift_clus_global_x.clear();
  m_ift_clus_global_y.clear();
  m_ift_clus_global_z.clear();
  m_ift_clus_fit_local_x.clear();
  m_ift_clus_fit_local_y.clear();
  m_ift_clus_fit_global_x.clear();
  m_ift_clus_fit_global_y.clear();


  m_ift_align_derivative_x_x.clear();
  m_ift_align_derivative_x_y.clear();
  m_ift_align_derivative_x_z.clear();
  m_ift_align_derivative_x_rx.clear();
  m_ift_align_derivative_x_ry.clear();
  m_ift_align_derivative_x_rz.clear();
  m_ift_align_derivative_x_par_x.clear();
  m_ift_align_derivative_x_par_y.clear();
  m_ift_align_derivative_x_par_theta.clear();
  m_ift_align_derivative_x_par_phi.clear();
  m_ift_align_derivative_x_par_qop.clear();
  m_ift_align_derivative_global_y_x.clear();
  m_ift_align_derivative_global_y_y.clear();
  m_ift_align_derivative_global_y_z.clear();
  m_ift_align_derivative_global_y_rx.clear();
  m_ift_align_derivative_global_y_ry.clear();
  m_ift_align_derivative_global_y_rz.clear();

  m_ift_align_derivative_y_x.clear();
  m_ift_align_derivative_y_y.clear();
  m_ift_align_derivative_y_z.clear();
  m_ift_align_derivative_y_rx.clear();
  m_ift_align_derivative_y_ry.clear();
  m_ift_align_derivative_y_rz.clear();
  m_ift_align_derivative_y_par_x.clear();
  m_ift_align_derivative_y_par_y.clear();
  m_ift_align_derivative_y_par_theta.clear();
  m_ift_align_derivative_y_par_phi.clear();
  m_ift_align_derivative_y_par_qop.clear();
  m_ift_align_derivative_global_x_x.clear();
  m_ift_align_derivative_global_x_y.clear();
  m_ift_align_derivative_global_x_z.clear();
  m_ift_align_derivative_global_x_rx.clear();
  m_ift_align_derivative_global_x_ry.clear();
  m_ift_align_derivative_global_x_rz.clear();

  m_ift_id.clear();
  m_ift_s0_strip.clear();
  m_ift_s1_strip.clear();
  m_ift_local_x.clear();
  m_ift_local_y.clear();
  m_ift_global_x.clear();
  m_ift_global_y.clear();
  m_ift_global_z.clear();
  m_ift_fit_local_x.clear();
  m_ift_fit_local_y.clear();
  m_ift_fit_global_x.clear();
  m_ift_fit_global_y.clear();
  m_ift_local_residual_x.clear();
  m_ift_local_residual_y.clear();
  m_ift_local_xe.clear();
  m_ift_local_ye.clear();
}


// double CKF2GlobalAlignment::getLocalDerivative(Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2)const {
//   double delta[6]={0.001,0.001,0.001,0.0001,0.0001,0.0001};
//   auto translation_m = Amg::Translation3D(0,0,0);
//   Amg::Transform3D transform_m= translation_m* Amg::RotationMatrix3D::Identity();
//   auto translation_p = Amg::Translation3D(0,0,0);
//   Amg::Transform3D transform_p= translation_p* Amg::RotationMatrix3D::Identity();
//   switch (ia)
//   {
//     case 0:
//       transform_m *= Amg::Translation3D(0-delta[ia],0,0);
//       transform_p *= Amg::Translation3D(0+delta[ia],0,0);
//       break;
//     case 1:
//       transform_m *= Amg::Translation3D(0,0-delta[ia],0);
//       transform_p *= Amg::Translation3D(0,0+delta[ia],0);
//       break;
//     case 2:
//       transform_m *= Amg::Translation3D(0,0,0-delta[ia]);
//       transform_p *= Amg::Translation3D(0,0,0+delta[ia]);
//       break;
//     case 3:
//       transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(1,0,0));
//       transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(1,0,0));
//       break;
//     case 4:
//       transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,1,0));
//       transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,1,0));
//       break;
//     case 5:
//       transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,0,1));
//       transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,0,1));
//       break;
//     default:
//       ATH_MSG_FATAL("Unknown alignment parameter" );
//       break;
//   }

//   auto local3 = Amg::Vector3D(loc_pos.x(),loc_pos.y(),0.);
//   Amg::Vector3D glo_pos = trans1 * local3;
//   Amg::Vector3D local_m = (trans1 * transform_m).inverse()*glo_pos;
//   Amg::Vector3D local_p = (trans1 * transform_p).inverse()*glo_pos;
//   if(side!=1)
//     return (local_p.x()-local_m.x())/2./delta[ia];
//   else{
//     Amg::Vector3D local_m2=trans2.inverse()*trans1*local_m;
//     Amg::Vector3D local_p2=trans2.inverse()*trans1*local_p;
//     return (local_p2.x()-local_m2.x())/2./delta[ia];
//   }
// }


double CKF2GlobalAlignment::getDerivative(const EventContext& ctx, Acts::BoundTrackParameters& fitParameters, Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int side , Amg::Transform3D trans2, int global)const {
  double delta[6]={0.001,0.001,0.001,0.0001,0.0001,0.0001};
  auto translation_m = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_m= translation_m* Amg::RotationMatrix3D::Identity();
  auto translation_p = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_p= translation_p* Amg::RotationMatrix3D::Identity();
  switch (ia)
  {
    case 0:
      transform_m *= Amg::Translation3D(0-delta[ia],0,0);
      transform_p *= Amg::Translation3D(0+delta[ia],0,0);
      break;
    case 1:
      transform_m *= Amg::Translation3D(0,0-delta[ia],0);
      transform_p *= Amg::Translation3D(0,0+delta[ia],0);
      break;
    case 2:
      transform_m *= Amg::Translation3D(0,0,0-delta[ia]);
      transform_p *= Amg::Translation3D(0,0,0+delta[ia]);
      break;
    case 3:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(1,0,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(1,0,0));
      break;
    case 4:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,1,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,1,0));
      break;
    case 5:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,0,1));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,0,1));
      break;
    default:
      ATH_MSG_FATAL("Unknown alignment parameter" );
      break;
  }

  auto local3 = Amg::Vector3D(loc_pos.x(),loc_pos.y(),0.);
  Amg::Vector3D glo_pos = trans2 * local3;
  auto trans_m = trans1 * transform_m;
  auto trans_p = trans1 * transform_p;
  if(side==1 && global!=1){
    trans_m = trans1 * transform_m * trans1.inverse() * trans2;
    trans_p = trans1 * transform_p * trans1.inverse() * trans2;
  }
  if (global==1){
    Amg::Transform3D trans_g = Amg::Translation3D(0,0, glo_pos.z()) * Amg::RotationMatrix3D::Identity();
    auto trans_l2g = trans_g.inverse()*trans1;
    trans_m = trans1 * trans_l2g.inverse() * transform_m * trans_l2g;
    trans_p = trans1 * trans_l2g.inverse() * transform_p * trans_l2g;
    if(side==1){
      auto tmpm = trans_m;
      auto tmpp = trans_p;
      trans_m = tmpm * trans1.inverse() * trans2;
      trans_p = tmpp * trans1.inverse() * trans2;
    }
  }

  Amg::Transform3D ini_trans = Amg::Translation3D(0,0, glo_pos.z()-10) * Amg::RotationMatrix3D::Identity();
  auto ini_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(ini_trans, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  std::unique_ptr<const Acts::BoundTrackParameters> ini_Param = m_extrapolationTool->propagate( ctx, fitParameters, *ini_surface, Acts::backward);
  if(ini_Param==nullptr)return -9999.;


  auto shift_m_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans_m, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  auto shift_p_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans_p, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  auto shift_m_Param = m_extrapolationTool->propagate( ctx, *ini_Param, *shift_m_surface, Acts::forward);
  auto shift_p_Param = m_extrapolationTool->propagate( ctx, *ini_Param, *shift_p_surface, Acts::forward);
  if(shift_m_Param!=nullptr&&shift_p_Param!=nullptr){
    auto shift_m_parameter = shift_m_Param->parameters();
    auto shift_p_parameter = shift_p_Param->parameters();
    Amg::Vector2D local_m(shift_m_parameter[Acts::eBoundLoc0],shift_m_parameter[Acts::eBoundLoc1]);
    Amg::Vector2D local_p(shift_p_parameter[Acts::eBoundLoc0],shift_p_parameter[Acts::eBoundLoc1]);
    return (local_p.x()-local_m.x())/2./delta[ia];
  }
  return -99999.;
}

double CKF2GlobalAlignment::getDerivativeOnParam(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, int ia, Amg::Transform3D trans2)const {

  double delta[5]={0.005,0.005,0.00001,0.00001,1.};
  auto target_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans2, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  Acts::BoundTrackParameters::ParametersVector param_m = fitParameters.parameters();
  Acts::BoundTrackParameters::ParametersVector param_p = fitParameters.parameters();
  double tmp_momentum = 1./param_m[Acts::eBoundQOverP];
  switch (ia)
  {
    case 0:
      param_m[Acts::eBoundLoc0] -= delta[ia];
      param_p[Acts::eBoundLoc0] += delta[ia];
      break;
    case 1:
      param_m[Acts::eBoundLoc1] -= delta[ia];
      param_p[Acts::eBoundLoc1] += delta[ia];
      break;
    case 2:
      // std::cout << "theta: " << param_m[Acts::eBoundTheta] << std::endl;
      param_m[Acts::eBoundTheta] -= delta[ia];
      param_p[Acts::eBoundTheta] += delta[ia];
      break;
    case 3:
      // std::cout << "phi: " << param_m[Acts::eBoundPhi] << std::endl;
      param_m[Acts::eBoundPhi] -= delta[ia];
      param_p[Acts::eBoundPhi] += delta[ia];
      break;
    case 4:
      // std::cout << "q/p: " << param_m[Acts::eBoundQOverP] << std::endl;
      // if the qop is almost 0 then use a smaller delta
      //      if(fabs(param_m[Acts::eBoundQOverP])<2*delta[ia]){
      // 	delta[ia]= 0.01*fabs(param_m[Acts::eBoundQOverP]);
      // 	param_m[Acts::eBoundQOverP] -= delta[ia];
      // 	param_p[Acts::eBoundQOverP] += delta[ia];
      //      }
      //      else{
      // 	param_m[Acts::eBoundQOverP] -= delta[ia];
      // 	param_p[Acts::eBoundQOverP] += delta[ia];
      //      }
      param_m[Acts::eBoundQOverP] = 1./(tmp_momentum-delta[ia]);
      param_p[Acts::eBoundQOverP] = 1./(tmp_momentum+delta[ia]);
      break;
    default:
      ATH_MSG_FATAL("Unknown track parameter" );
      break;
  }
  // std::cout<<"like para deri "<<ia<<std::endl;
  // std::cout<<"ini params:   " << fitParameters.parameters().transpose()<<std::endl;
  // std::cout<<"m params:   " << param_m.transpose()<<std::endl;
  // std::cout<<"p params:   " << param_p.transpose()<<std::endl;

  Acts::BoundTrackParameters shift_m_ini_parameter( fitParameters.referenceSurface().getSharedPtr(), param_m, fitParameters.covariance());
  Acts::BoundTrackParameters shift_p_ini_parameter( fitParameters.referenceSurface().getSharedPtr(), param_p, fitParameters.covariance());
  // std::cout << "shift_m_ini_parameter: " << shift_m_ini_parameter.parameters() << std::endl;
  // std::cout << "shift_p_ini_parameter: " << shift_p_ini_parameter.parameters() << std::endl;
  // std::cout << "ini_parameter_diff" << shift_p_ini_parameter.parameters().x()-shift_m_ini_parameter.parameters().x() << std::endl;

  auto shift_m_Param = m_extrapolationTool->propagate( ctx, shift_m_ini_parameter, *target_surface, Acts::forward);
  auto shift_p_Param = m_extrapolationTool->propagate( ctx, shift_p_ini_parameter, *target_surface, Acts::forward);
  // std::cout << "shift_m_Param: " << shift_m_Param->parameters() << std::endl;
  // std::cout << "shift_p_Param: " << shift_p_Param->parameters() << std::endl;

  if(shift_m_Param!=nullptr&&shift_p_Param!=nullptr){
    auto shift_m_parameter = shift_m_Param->parameters();
    auto shift_p_parameter = shift_p_Param->parameters();
    Amg::Vector2D local_m(shift_m_parameter[Acts::eBoundLoc0],shift_m_parameter[Acts::eBoundLoc1]);
    Amg::Vector2D local_p(shift_p_parameter[Acts::eBoundLoc0],shift_p_parameter[Acts::eBoundLoc1]);
    // std::cout<<"like local "<<local_p.x()<<" "<<local_m.x()<<" diff: "<<local_p.x()-local_m.x() << " delat: " << delta[ia]<<std::endl;
    return (local_p.x()-local_m.x())/2./delta[ia];
  }
  return -99999.;
}

// For IFT
std::pair<double, double> CKF2GlobalAlignment::IFTgetDerivative(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, Amg::Transform3D trans1, Amg::Vector2D loc_pos, int ia, int global)const {
  double delta[6]={0.001,0.001,0.001,0.0001,0.0001,0.0001};
  auto translation_m = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_m= translation_m* Amg::RotationMatrix3D::Identity();
  auto translation_p = Amg::Translation3D(0,0,0);
  Amg::Transform3D transform_p= translation_p* Amg::RotationMatrix3D::Identity();
  switch (ia)
  {
    case 0:
      transform_m *= Amg::Translation3D(0-delta[ia],0,0);
      transform_p *= Amg::Translation3D(0+delta[ia],0,0);
      break;
    case 1:
      transform_m *= Amg::Translation3D(0,0-delta[ia],0);
      transform_p *= Amg::Translation3D(0,0+delta[ia],0);
      break;
    case 2:
      transform_m *= Amg::Translation3D(0,0,0-delta[ia]);
      transform_p *= Amg::Translation3D(0,0,0+delta[ia]);
      break;
    case 3:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(1,0,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(1,0,0));
      break;
    case 4:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,1,0));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,1,0));
      break;
    case 5:
      transform_m *= Amg::AngleAxis3D(0-delta[ia], Amg::Vector3D(0,0,1));
      transform_p *= Amg::AngleAxis3D(0+delta[ia], Amg::Vector3D(0,0,1));
      break;
    default:
      ATH_MSG_FATAL("Unknown alignment parameter" );
      break;
  }

  auto local3 = Amg::Vector3D(loc_pos.x(),loc_pos.y(),0.);
  Amg::Vector3D glo_pos = trans1 * local3;
  auto trans_m = trans1 * transform_m;
  auto trans_p = trans1 * transform_p;
  if (global==1) {
    Amg::Transform3D trans_g = Amg::Translation3D(0,0, glo_pos.z()) * Amg::RotationMatrix3D::Identity();
    auto trans_l2g = trans_g.inverse()*trans1;
    trans_m = trans1 * trans_l2g.inverse() * transform_m * trans_l2g;
    trans_p = trans1 * trans_l2g.inverse() * transform_p * trans_l2g;
  }

  Amg::Transform3D ini_trans = Amg::Translation3D(0,0, glo_pos.z()-10) * Amg::RotationMatrix3D::Identity();
  auto ini_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(ini_trans, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  std::unique_ptr<const Acts::BoundTrackParameters> ini_Param = m_extrapolationTool->propagate( ctx, fitParameters, *ini_surface, Acts::backward);
  if(ini_Param==nullptr){
    return std::make_pair(-9999., -9999.);
  }


  auto shift_m_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans_m, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  auto shift_p_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans_p, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  auto shift_m_Param = m_extrapolationTool->propagate( ctx, *ini_Param, *shift_m_surface, Acts::forward);
  auto shift_p_Param = m_extrapolationTool->propagate( ctx, *ini_Param, *shift_p_surface, Acts::forward);
  if(shift_m_Param!=nullptr&&shift_p_Param!=nullptr){
    auto shift_m_parameter = shift_m_Param->parameters();
    auto shift_p_parameter = shift_p_Param->parameters();
    Amg::Vector2D local_m(shift_m_parameter[Acts::eBoundLoc0],shift_m_parameter[Acts::eBoundLoc1]);
    Amg::Vector2D local_p(shift_p_parameter[Acts::eBoundLoc0],shift_p_parameter[Acts::eBoundLoc1]);
    return std::make_pair((local_p.x()-local_m.x())/2./delta[ia], (local_p.y()-local_m.y())/2./delta[ia]);
  }
  return std::make_pair(-9999., -9999.);
}

std::pair<double, double> CKF2GlobalAlignment::IFTgetDerivativeOnParam(const EventContext& ctx, const Acts::BoundTrackParameters& fitParameters, int ia, Amg::Transform3D trans2)const {
  double delta[5]={0.005,0.005,0.00001,0.00001,1.};
  auto target_surface = Acts::Surface::makeShared<Acts::PlaneSurface>(trans2, std::make_shared<const Acts::RectangleBounds>(1000.,1000.));
  Acts::BoundTrackParameters::ParametersVector param_m = fitParameters.parameters();
  Acts::BoundTrackParameters::ParametersVector param_p = fitParameters.parameters();
  double tmp_momentum = 1./param_m[Acts::eBoundQOverP];
  // std::cout << "####################################################" << std::endl;
  switch (ia)
  {
    case 0:
      param_m[Acts::eBoundLoc0] -= delta[ia];
      param_p[Acts::eBoundLoc0] += delta[ia];
      break;
    case 1:
      param_m[Acts::eBoundLoc1] -= delta[ia];
      param_p[Acts::eBoundLoc1] += delta[ia];
      break;
    case 2:
      // std::cout << "IFTTTTTT theta: " << param_m[Acts::eBoundTheta] << std::endl;
      param_m[Acts::eBoundTheta] -= delta[ia];
      param_p[Acts::eBoundTheta] += delta[ia];
      break;
    case 3:
      // std::cout << "IFTTTT PHI: " << param_m[Acts::eBoundPhi] << std::endl;
      param_m[Acts::eBoundPhi] -= delta[ia];
      param_p[Acts::eBoundPhi] += delta[ia];
      break;
    case 4:
      // std::cout << "IFTTTT q/p: " << param_m[Acts::eBoundQOverP] << std::endl;
      param_m[Acts::eBoundQOverP] = 1./(tmp_momentum-delta[ia]);
      param_p[Acts::eBoundQOverP] = 1./(tmp_momentum+delta[ia]);
      break;
    default:
      ATH_MSG_FATAL("Unknown track parameter" );
      break;
  }
  //  std::cout<<"like para deri "<<ia<<std::endl;
  // std::cout<<"IFT ini params:   " << fitParameters.parameters().transpose()<<std::endl;
  // std::cout<<"IFT m params:   " << param_m.transpose()<<std::endl;
  // std::cout<<"IFT p params:   " << param_p.transpose()<<std::endl;

  Acts::BoundTrackParameters shift_m_ini_parameter( fitParameters.referenceSurface().getSharedPtr(), param_m, fitParameters.covariance());
  Acts::BoundTrackParameters shift_p_ini_parameter( fitParameters.referenceSurface().getSharedPtr(), param_p, fitParameters.covariance());
  // std::cout << "IFT shift_m_ini_parameter: " << shift_m_ini_parameter.parameters() << std::endl;
  // std::cout << "IFT shift_p_ini_parameter: " << shift_p_ini_parameter.parameters() << std::endl;
  // std::cout << "ini_parameter_diff" << shift_p_ini_parameter.parameters().x()-shift_m_ini_parameter.parameters().x() << std::endl;
  auto shift_m_Param = m_extrapolationTool->propagate( ctx, shift_m_ini_parameter, *target_surface, Acts::backward);
  auto shift_p_Param = m_extrapolationTool->propagate( ctx, shift_p_ini_parameter, *target_surface, Acts::backward);
  // std::cout << "IFT shift_m_Param: " << shift_m_Param->parameters() << std::endl;
  //std::cout << "IFT shift_p_Param: " << shift_p_Param->parameters() << std::endl;
  if(shift_m_Param!=nullptr&&shift_p_Param!=nullptr){
    auto shift_m_parameter = shift_m_Param->parameters();
    auto shift_p_parameter = shift_p_Param->parameters();
    Amg::Vector2D local_m(shift_m_parameter[Acts::eBoundLoc0],shift_m_parameter[Acts::eBoundLoc1]);
    Amg::Vector2D local_p(shift_p_parameter[Acts::eBoundLoc0],shift_p_parameter[Acts::eBoundLoc1]);
    // std::cout<<"IFT like local "<<local_p.x()<<" "<<local_m.x()<<" diff: "<<local_p.x()-local_m.x() << " delat: " << delta[ia]<<std::endl;
    return std::make_pair((local_p.x()-local_m.x())/2./delta[ia], (local_p.y()-local_m.y())/2./delta[ia]);
  }
  return std::make_pair(-9999., -9999.);
  // std::cout << "####################################################" << std::endl;

}
