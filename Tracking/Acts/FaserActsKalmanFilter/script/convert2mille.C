#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>
#include <boost/iostreams/filtering_stream.hpp>


using namespace boost::iostreams;

class MilleDataWriter {
  static_assert(std::is_same<double, double>::value || std::is_same<double, float>::value,
      "Can only write floats or doubles.");

  std::string filename;
  std::ofstream file_stream;
  filtering_ostream gz_fstream;

  std::vector<double> track_buf;
  std::vector<int> label_buf;

  public:
  MilleDataWriter(const std::string& file, int buf_size = 1000) :
    filename(file), file_stream(file, std::ios_base::out | std::ios_base::binary) {
      gz_fstream.push(file_stream);

      track_buf.reserve(buf_size);
      label_buf.reserve(buf_size);
    }

  void pushHit(std::vector<double>const & local_derivatives,
      std::vector<double>const & global_derivatives,
      std::vector<int>const & global_labels, double const& measurement,
      double const& meas_error) {

    if (meas_error < 0) {
      std::cerr << "MilleDataWriter: "
	<< "Bad measurement" << std::endl;
      return;
    }

    if (track_buf.size() == label_buf.size() && track_buf.size() == 0) {
      push(0.0, 0);
    }
    push(measurement, 0);

    std::vector<int> local_labels(local_derivatives.size());
    std::iota(local_labels.begin(), local_labels.end(), 1);
    push(local_derivatives, local_labels);

    push(meas_error, 0);
    push(global_derivatives, global_labels);
  }

  void flushTrack() {
    int words = n_words();
    gz_fstream.write(reinterpret_cast<char*>(&words), sizeof(int));
    gz_fstream.write(reinterpret_cast<char*>(track_buf.data()), track_buf.size() * sizeof(double));
    gz_fstream.write(reinterpret_cast<char*>(label_buf.data()), label_buf.size() * sizeof(int));
    clear();
  }

  void clear() {
    label_buf.clear();
    track_buf.clear();
  }

  private:
  void push(std::vector<double> const& data, std::vector<int> const& labels) {
    if (data.size() != labels.size()) {
      std::cerr << "MilleDataWriter: size mismatch between data (" << data.size()
	<< ") and labels (" << labels.size() << ")" << std::endl;
      return;
    }
    label_buf.insert(label_buf.end(), labels.begin(), labels.end());
    track_buf.insert(track_buf.end(), data.begin(), data.end());
  }

  void push(double const& data, int const& label) {
    label_buf.emplace_back(label);
    track_buf.emplace_back(data);
  }

  int n_words() {
    int result = label_buf.size() + track_buf.size();

    // if we are storing doubles, we pass a negative word number
    // to indicate to readC(..) (readc.c) it should read doubles, not floats.
    if (std::is_same<double, double>::value) {
      result = -result;
    }

    return result;
  }
};

void convert2mille(){
  TFile* f1=new TFile("/afs/cern.ch/user/k/keli/eos/Faser/alignment/global/misalign_MC/kfalignment_mc_iter0.root");
  TTree* t1=(TTree*)f1->Get("trackParam");

  double m_fitParam_x=0;
  double m_fitParam_y=0;
  double m_fitParam_pull_x=0;
  double m_fitParam_pull_y=0;
  double m_fitParam_chi2=0;
  double m_fitParam_ndf=0;
  double m_fitParam_nMeasurements=0;
  double m_fitParam_px=0;
  double m_fitParam_py=0;
  double m_fitParam_pz=0;
  double m_fitParam_charge=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_x=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_y=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_z=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_rx=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_ry=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_rz=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_x=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_y=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_z=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_rx=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_ry=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_rz=0;
  std::vector<double>* m_fitParam_align_local_residual_x=0;
  std::vector<double>* m_fitParam_align_local_measured_x=0;
  std::vector<double>* m_fitParam_align_local_measured_xe=0;
  std::vector<double>* m_fitParam_align_id=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_x=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_y=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_theta=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_phi=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_qop=0;
  t1->SetBranchAddress("fitParam_x",&m_fitParam_x);
  t1->SetBranchAddress("fitParam_y",&m_fitParam_y);
  t1->SetBranchAddress("fitParam_chi2",&m_fitParam_chi2);
  t1->SetBranchAddress("fitParam_ndf",&m_fitParam_ndf);
  t1->SetBranchAddress("fitParam_nMeasurements",&m_fitParam_nMeasurements);
  t1->SetBranchAddress("fitParam_pz",&m_fitParam_pz);
  t1->SetBranchAddress("fitParam_px",&m_fitParam_px);
  t1->SetBranchAddress("fitParam_py",&m_fitParam_py);
  t1->SetBranchAddress("fitParam_charge",&m_fitParam_charge);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_x",&m_fitParam_align_global_derivation_y_x);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_y",&m_fitParam_align_global_derivation_y_y);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_z",&m_fitParam_align_global_derivation_y_z);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_rx",&m_fitParam_align_global_derivation_y_rx);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_ry",&m_fitParam_align_global_derivation_y_ry);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_rz",&m_fitParam_align_global_derivation_y_rz);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_x",&m_fitParam_align_local_derivation_x_x);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_y",&m_fitParam_align_local_derivation_x_y);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_z",&m_fitParam_align_local_derivation_x_z);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_rx",&m_fitParam_align_local_derivation_x_rx);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_ry",&m_fitParam_align_local_derivation_x_ry);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_rz",&m_fitParam_align_local_derivation_x_rz);
  t1->SetBranchAddress("fitParam_align_local_residual_x",&m_fitParam_align_local_residual_x);
  t1->SetBranchAddress("fitParam_align_local_measured_x",&m_fitParam_align_local_measured_x);
  t1->SetBranchAddress("fitParam_align_local_measured_xe",&m_fitParam_align_local_measured_xe);
  t1->SetBranchAddress("fitParam_align_id",&m_fitParam_align_id);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_x",&m_fitParam_align_local_derivation_x_par_x);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_y",&m_fitParam_align_local_derivation_x_par_y);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_theta",&m_fitParam_align_local_derivation_x_par_theta);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_phi",&m_fitParam_align_local_derivation_x_par_phi);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_qop",&m_fitParam_align_local_derivation_x_par_qop);

  int nevt= t1->GetEntries();

  MilleDataWriter mille_file("mp2input.bin");

  //loop over all the events
  std::vector<int> labels;
  std::vector<double> glo_der;
  std::vector<double> loc_der;

  for(int ievt = 0;ievt< nevt; ++ievt){
    if(ievt>10000)continue;
    //t1->GetEntry(ievt);
    std::cout<<"like "<<ievt<<" "<<m_fitParam_chi2<<" "<<m_fitParam_pz<<" "<<m_fitParam_align_id->size()<<std::endl;
    if(m_fitParam_chi2>100||m_fitParam_pz<100||m_fitParam_pz>5000||m_fitParam_align_id->size()<15)continue;
    for(int ihit = 0; ihit<m_fitParam_align_id->size();++ihit){
      labels.clear();
      glo_der.clear();
      loc_der.clear();
      labels.push_back(m_fitParam_align_id->at(ihit)*10+0);
      glo_der.push_back(m_fitParam_align_local_derivation_x_par_x->at(ihit));
      labels.push_back(m_fitParam_align_id->at(ihit)*10+1);
      glo_der.push_back(m_fitParam_align_local_derivation_x_par_y->at(ihit));
      labels.push_back(m_fitParam_align_id->at(ihit)*10+2);
      glo_der.push_back(m_fitParam_align_local_derivation_x_par_theta->at(ihit));
      labels.push_back(m_fitParam_align_id->at(ihit)*10+3);
      glo_der.push_back(m_fitParam_align_local_derivation_x_par_phi->at(ihit));
      labels.push_back(m_fitParam_align_id->at(ihit)*10+4);
      glo_der.push_back(m_fitParam_align_local_derivation_x_par_qop->at(ihit));
      loc_der.push_back(m_fitParam_align_local_derivation_x_x->at(ihit));
      loc_der.push_back(m_fitParam_align_local_derivation_x_y->at(ihit));
      loc_der.push_back(m_fitParam_align_local_derivation_x_z->at(ihit));
      loc_der.push_back(m_fitParam_align_local_derivation_x_rx->at(ihit));
      loc_der.push_back(m_fitParam_align_local_derivation_x_ry->at(ihit));
      loc_der.push_back(m_fitParam_align_local_derivation_x_rz->at(ihit));
      std::cout<<"like "<<ievt<<" "<<ihit<<" "<<loc_der.size()<<std::endl;
      mille_file.pushHit(loc_der, glo_der, labels, m_fitParam_align_local_residual_x->at(ihit), m_fitParam_align_local_measured_xe->at(ihit));
    }
    mille_file.flushTrack();
  }


}
