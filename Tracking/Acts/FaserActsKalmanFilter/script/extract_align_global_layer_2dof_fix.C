#include "TFile.h"
#include "TMatrix.h"
#include "TMatrixD.h"
#include "TTree.h"
#include <fstream>
#include <iostream>
#include <cmath>

void iniMatrix(TMatrixD& m, int d1, int d2){
  for(int i=0;i<d1;i++){
    for(int j=0;j<d2;j++){
    }
  }
}

int main(int argc, char *argv[]){
  TString outputpath="/eos/home-k/keli/Faser/alignment/global/8023_8025_8115_8301_8730_9073";
  TString outputname="data_iter";
  outputname+=argv[1];
  std::cout<<"like open "<<outputname<<std::endl;
  int fix1=11;
  int fix2=31;
  TFile* f1=new TFile(outputpath+"/kfalignment_"+outputname+".root");
  TTree* t1=(TTree*)f1->Get("trackParam");
  double m_fitParam_chi2=0;
  double m_fitParam_pz=0;
  int m_fitParam_ndf=0;
  int m_fitParam_nMeasurements=0;
  std::vector<double>* m_fitParam_align_local_residual_x=0;
  std::vector<double>* m_fitParam_align_local_measured_x=0;
  std::vector<double>* m_fitParam_align_local_measured_xe=0;
  std::vector<double>* m_fitParam_align_local_fitted_x=0;
  std::vector<double>* m_fitParam_align_local_fitted_y=0;
  std::vector<double>* m_fitParam_align_id=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_x=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_y=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_theta=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_phi=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_par_qop=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_x=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_y=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_z=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_rx=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_ry=0;
  std::vector<double>* m_fitParam_align_local_derivation_x_rz=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_x=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_y=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_z=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_rx=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_ry=0;
  std::vector<double>* m_fitParam_align_global_derivation_y_rz=0;
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_x",&m_fitParam_align_local_derivation_x_par_x);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_y",&m_fitParam_align_local_derivation_x_par_y);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_theta",&m_fitParam_align_local_derivation_x_par_theta);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_phi",&m_fitParam_align_local_derivation_x_par_phi);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_par_qop",&m_fitParam_align_local_derivation_x_par_qop);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_x",&m_fitParam_align_local_derivation_x_x);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_y",&m_fitParam_align_local_derivation_x_y);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_z",&m_fitParam_align_local_derivation_x_z);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_rx",&m_fitParam_align_local_derivation_x_rx);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_ry",&m_fitParam_align_local_derivation_x_ry);
  t1->SetBranchAddress("fitParam_align_local_derivation_x_rz",&m_fitParam_align_local_derivation_x_rz);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_x",&m_fitParam_align_global_derivation_y_x);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_y",&m_fitParam_align_global_derivation_y_y);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_z",&m_fitParam_align_global_derivation_y_z);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_rx",&m_fitParam_align_global_derivation_y_rx);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_ry",&m_fitParam_align_global_derivation_y_ry);
  t1->SetBranchAddress("fitParam_align_global_derivation_y_rz",&m_fitParam_align_global_derivation_y_rz);
  t1->SetBranchAddress("fitParam_align_local_residual_x",&m_fitParam_align_local_residual_x);
  t1->SetBranchAddress("fitParam_chi2",&m_fitParam_chi2);
  t1->SetBranchAddress("fitParam_ndf",&m_fitParam_ndf);
  t1->SetBranchAddress("fitParam_nMeasurements",&m_fitParam_nMeasurements);
  t1->SetBranchAddress("fitParam_pz",&m_fitParam_pz);
  t1->SetBranchAddress("fitParam_align_local_measured_x",&m_fitParam_align_local_measured_x);
  t1->SetBranchAddress("fitParam_align_local_measured_xe",&m_fitParam_align_local_measured_xe);
  t1->SetBranchAddress("fitParam_align_local_fitted_x",&m_fitParam_align_local_fitted_x);
  t1->SetBranchAddress("fitParam_align_id",&m_fitParam_align_id);
  //define matrix
  int Ndim=2;
  int Nmod=2;
  int Npar=5;
  double inte = 1.;
//  TMatrixD mat_cov_inve(Nmod,Nmod);
  TMatrixD mat_m(Nmod*Ndim,Nmod*Ndim);
  TMatrixD mat_v(Nmod*Ndim,1);
  //  iniMatrix(mat_m, Nmod*Ndim, Nmod*Ndim);
  //  iniMatrix(mat_v, Nmod*Ndim, 1);
  //  iniMatrix(mat_cov, Nmod, Nmod);
//  for(int i=0;i<Nmod;i++){
//    mat_cov_inve[i][i]=0.02*0.02;
//  }
//  mat_cov_inve.Invert(&inte);

  int nevt=t1->GetEntries();
  int ngoodtrack=0;
  std::cout<<"found "<<nevt<<" events "<<std::endl;
  //loop over all the entries
//  for(int ievt=0;ievt<10;ievt++){
  for(int ievt=0;ievt<nevt;ievt++){
    t1->GetEntry(ievt);
    if(ievt%10000==0)std::cout<<"processing "<<ievt<<" event"<<std::endl;
    if(m_fitParam_chi2>100||m_fitParam_pz<100||m_fitParam_pz>5000||m_fitParam_nMeasurements<15)continue;
    //construct the matrix for each track
    int Nclus=0;
    int ndupli=0;
    bool badtrack=false;
    std::vector<int> moduleId;
    for(int j=0;j<m_fitParam_align_local_residual_x->size();++j){
      double residual=m_fitParam_align_local_residual_x->at(j);
      if(std::fabs(residual)>0.2) continue;
      int id=(int)m_fitParam_align_id->at(j);
      int moduleid=id/1000*3+id%1000/100;
	int layerid=id/100;
	if(layerid!=fix1&&layerid!=fix2)continue;
      if(std::fabs(m_fitParam_align_local_derivation_x_par_x->at(j)-1)>2||std::fabs(m_fitParam_align_local_derivation_x_par_y->at(j))>5||std::fabs(m_fitParam_align_local_derivation_x_par_theta->at(j))>6000||std::fabs(m_fitParam_align_local_derivation_x_par_phi->at(j))>600||std::fabs(m_fitParam_align_local_derivation_x_par_qop->at(j))>5)
	badtrack=true;
      if(Nclus>0&&moduleId[Nclus-1]==moduleid)++ndupli;
      moduleId.push_back(moduleid);
      ++Nclus;
    }
    if(badtrack)continue;
    ++ngoodtrack;
    int Nmodule=Nclus - ndupli;
    if(Nmodule!=2)continue;
//    int moduleId[Nclus];
    TMatrixD mat_resi(Nclus, 1);
    TMatrixD mat_deri_align(Nclus, Nmodule*Ndim);
    TMatrixD newmat_deri_align(Nclus, Nmodule*Ndim);
    TMatrixD mat_deri_par(Nclus, Npar);
    TMatrixD mat_w(Nclus, Nclus);
    TMatrixD mat_deri_align_t(Nmodule*Ndim, Nclus);
    TMatrixD newmat_deri_align_t(Nmodule*Ndim, Nclus);
    TMatrixD mat_deri_par_t(Npar, Nclus);
    TMatrixD mat_cov_inve(Nclus, Nclus);
    for(int i=0;i<Nclus;++i){
      mat_cov_inve[i][i]=0.02*0.02;
    }
    inte=1.;
    mat_cov_inve.Invert(&inte);
    Nclus=0;
    Nmodule=0;
    moduleId.clear();
    for(int j=0;j<m_fitParam_align_local_residual_x->size();++j){
      double residual=m_fitParam_align_local_residual_x->at(j);
      if(std::fabs(residual)>0.2) continue;
      int id=(int)m_fitParam_align_id->at(j);
      int moduleid=id/1000*3+id%1000/100;

      //check if the module is the same with the previous clusters
      if(Nmodule>0&&moduleId[Nmodule-1]==moduleid)--Nmodule;
      else
      moduleId[Nmodule]=moduleid;

      //fill the matrix
      mat_resi[Nclus][0]=residual;
      mat_deri_align[Nclus][Nmodule*Ndim+0]=m_fitParam_align_global_derivation_y_y->at(j);
      mat_deri_align[Nclus][Nmodule*Ndim+1]=m_fitParam_align_global_derivation_y_rz->at(j);
      mat_deri_par[Nclus][0]=m_fitParam_align_local_derivation_x_par_x->at(j);
      mat_deri_par[Nclus][1]=m_fitParam_align_local_derivation_x_par_y->at(j);
      mat_deri_par[Nclus][2]=m_fitParam_align_local_derivation_x_par_theta->at(j);
      mat_deri_par[Nclus][3]=m_fitParam_align_local_derivation_x_par_phi->at(j);
      mat_deri_par[Nclus][4]=m_fitParam_align_local_derivation_x_par_qop->at(j);
      ++Nclus;
      ++Nmodule;
    }
    mat_deri_align_t.Transpose( mat_deri_align );
    mat_deri_par_t.Transpose( mat_deri_par );
    inte=1.;
    mat_w = mat_cov_inve ;
    TMatrixD mat_tmp = mat_deri_par_t * mat_cov_inve * mat_deri_par;
    mat_tmp.Invert(&inte);
    newmat_deri_align = mat_deri_align - mat_deri_par * mat_tmp * mat_deri_par_t * mat_cov_inve * mat_deri_align ;
    TMatrixD newmat = newmat_deri_align  -mat_deri_align;
    newmat_deri_align_t.Transpose( newmat_deri_align );
//    mat_w = mat_cov_inve - mat_cov_inve*mat_deri_par*mat_tmp*mat_deri_par_t*mat_cov_inve;

//    TMatrixD tmp_m = mat_deri_align_t* mat_w * mat_deri_align;
//    TMatrixD tmp_v = mat_deri_align_t* mat_w * mat_resi;
    TMatrixD tmp_m = newmat_deri_align_t* mat_w * newmat_deri_align;
    TMatrixD tmp_v = newmat_deri_align_t* mat_w * mat_resi;
    for(int i=0;i<Nmodule;i++){
      int moduleid_x=moduleId[i]*Ndim;
      mat_v[moduleid_x+0][0]+=tmp_v[i*Ndim+0][0];
      mat_v[moduleid_x+1][0]+=tmp_v[i*Ndim+1][0];
      for(int j=0;j<Nmodule;j++){
      int moduleid_y=moduleId[j]*Ndim;
      //std::cout<<"like "<<i<<" "<<j<<" "<<moduleid_x<<" "<<moduleid_y<<std::endl;
      mat_m[moduleid_x+0][moduleid_y]+=tmp_m[i*Ndim+0][j*Ndim];
      mat_m[moduleid_x+1][moduleid_y]+=tmp_m[i*Ndim+1][j*Ndim];
      mat_m[moduleid_x+0][moduleid_y+1]+=tmp_m[i*Ndim+0][j*Ndim+1];
      mat_m[moduleid_x+1][moduleid_y+1]+=tmp_m[i*Ndim+1][j*Ndim+1];
      }
    }
  //  mat_m+=newmat_deri_align_t* mat_w * newmat_deri_align;
  //  mat_v+=newmat_deri_align_t* mat_w * mat_resi;
  }
  std::cout<<"like finish reading"<<std::endl;

  std::cout<<"Get the nominal transform"<<std::endl;
  //get the alignment constants
  //  std::ofstream align_output("all_alignment_"+outputname+".txt",std::ios::out);
  std::ofstream alignparam_output("all_alignment_input_"+outputname+".txt",std::ios::out);

  inte=1.;

  mat_m.Invert(&inte);

  TMatrixD alignpar= mat_m * mat_v;
  for(int i=0;i<Nmod*Ndim;i++){
    if(i%Ndim==0)
      alignparam_output<<i/Ndim/3<<i/Ndim%3<<" 0 "<<alignpar[i][0]<<" 0 0 0";
    else
      alignparam_output<<" "<<alignpar[i][0];
    if(i%Ndim==(Ndim-1))
      alignparam_output<<std::endl;

    //alignparam_output<<i/Ndim/3<<i/Ndim%3<<" "<<alignpar[i][0]<<" "<<sqrt(mat_m[i][i])<<std::endl;
  }


  std::cout<<"closing the output"<<std::endl;
  std::cout<<"found "<<ngoodtrack<<" good tracks from "<<nevt<<" events "<<std::endl;
  alignparam_output.close();
  return 0;
  //  align_output.close();
  }
