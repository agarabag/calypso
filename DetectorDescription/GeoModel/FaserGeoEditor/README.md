# FaserGeoEditorApp
Qt Application for editing and maintaining the Faser GeoModel Sql Database

After following calypso readme to build all files:

cd ../run

source ./setup.sh

The program can currently read a sql file of text commands, or a sql binary file. To differentiate between the file being read, add a third argument to call the program of t for text, b for binary
Note: it cannot read sqlite files currently

ex:
FaserGeoEditor data/geomDB.sql t

Adding rows/columns adds one after the selected index (if none selected inputs new one at beginning)

Changes to table are only saved if submitted

While open the file, along with changes will continuously saved to a binary file GeoEditorSqlite

To finalize the file and resave as a text file, ctrl+s or save in the menu will allow for this

Color coding : 
	green -associated with an unlocked tag; data can be edited and children tags can be created(if branch)
	yellow -associated with a doubly unlocked tag; in general this can lead to unwanted behaviour, and may cause issues if continued. When one of the tags associated is later locked, it will still lock the data but can lead to issues with child tag creations
	red -associated with a locked tag. Data cannot be edited and child tags cannot be added. To change data or create children, need to create a new tag at this level that is a copy of the version desired, then create children.
