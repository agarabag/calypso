// EDM include(s):
#include "xAODCore/AddDVProxy.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticleContainer.h"
#include "xAODFaserTruth/FaserTruthVertexContainer.h"
#include "xAODFaserTruth/FaserTruthEventContainer.h"

// Set up the collection proxies:
ADD_NS_DV_PROXY( xAOD, FaserTruthParticleContainer );
ADD_NS_DV_PROXY( xAOD, FaserTruthVertexContainer );
ADD_NS_DV_PROXY( xAOD, FaserTruthEventContainer );
