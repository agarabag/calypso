// Local include(s):
#include "xAODFaserTruth/FaserTruthParticleAuxContainer.h"

namespace xAOD {

   FaserTruthParticleAuxContainer::FaserTruthParticleAuxContainer()
   : AuxContainerBase() {

      AUX_VARIABLE( pdgId );
      AUX_VARIABLE( barcode );
      AUX_VARIABLE( status );
      AUX_VARIABLE( prodVtxLink );
      AUX_VARIABLE( decayVtxLink );
      AUX_VARIABLE( px );
      AUX_VARIABLE( py );
      AUX_VARIABLE( pz );
      AUX_VARIABLE( e );
      AUX_VARIABLE( m );
   }

} // namespace xAOD
