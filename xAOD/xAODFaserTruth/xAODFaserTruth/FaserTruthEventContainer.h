// -*- C++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthEventContainer_v1.h 622193 2014-10-16 16:08:34Z krasznaa $
#ifndef XAODFASERTRUTH_TRUTHEVENTCONTAINER_H
#define XAODFASERTRUTH_TRUTHEVENTCONTAINER_H

// EDM include(s):
#include "AthContainers/DataVector.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthEvent.h"

namespace xAOD {
   /// Alias
   typedef DataVector< FaserTruthEvent > FaserTruthEventContainer;
}

// Declare a CLID for the class for Athena:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTruthEventContainer, 1237345824, 1 )

#endif // XAODFASERTRUTH_TRUTHEVENTCONTAINER_H
