################################################################################
# Package: FaserEventStorage
################################################################################

# Declare the package name:
atlas_subdir( FaserEventStorage )

# External dependencies:
find_package( tdaq-common COMPONENTS ers EventStorage )
find_package( Boost COMPONENTS system )

atlas_add_library( FaserEventStorageLib
    FaserEventStorage/*.h src/*.h src/*.cxx
    PUBLIC_HEADERS FaserEventStorage
    INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${TDAQ-COMMON_INCLUDE_DIRS}
    LINK_LIBRARIES ${CMAKE_DL_LIBS} ${Boost_LIBRARIES} ${TDAQ-COMMON_LIBRARIES} EventFormats )



# atlas_add_library(fReadPlain 
#      src/fReadPlain.h src/fReadPlain.cxx 
#      NO_PUBLIC_HEADERS
#      PRIVATE_INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${TDAQ-COMMON_INCLUDE_DIRS}
#      PRIVATE_LINK_LIBRARIES ${Boost_LIBRARIES} ${TDAQ-COMMON_LIBRARIES} -rdynamic)

# Install files from the package:
#atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
#atlas_install_joboptions( share/*.py )
